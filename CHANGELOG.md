# Changelog
All notable changes to this project will be documented in this file.
Note: Changelogs are for HUMANS, not computers.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.22.0] - 2020-09-10
## Added
- AuthenticationClient that supports login requests with emailaddress & password, through an Akamai Login Client.

## [0.21.2] - 2020-07-30
## Changed
- the value field for the call to akamai was incorrect, it should be attributes

## [0.21.1] - 2020-07-29
## Changed
- added info to the invalid response from akamai

## [0.21] - 2020-07-09
## Changed
- Modified the defaults of the isLite, isActive and isRegistered booleans in the AccountDtoMapper, since the defaults
  were set for lite accounts, but we have our own LiteAccountDTOMapper now for those.

## [0.20.1] - 2020-07-09
## Changed
- Modified regex used in the process of validating the JSON Web Token to fix a bug that emerges when logging in through social

## [0.20.0] - 2020-07-01
## Added
- Bio DTO and DTO mapper to allow retrieving and updating the bio field individually

## [0.19.1] - 2020-07-01
## Added
- Bugfix, restored missing categories element

## [0.19.0] - 2020-07-01
## Added
- Added the Social Account Dto + Mapper. For the Deprecated Social Accounts in Akamai.

## [0.18.2] - 2020-06-30
## Changed
- Fixed a couple of issues with Communities DTOs being mapped incorrectly

## [0.18.1] - 2020-06-30
## Added
- Missing category for Kassa
- Methods to the Akamai Client Interface

## [0.18.0] - 2020-06-25
## Added
- The ability to retrieve "communities" field individually as a single field dto

## [0.17.0] - 2020-06-17
## Added
- The ability to update the "communities" field individually
- Throw an exception upon encountering an expired json web token

## [0.16.1] - 2020-06-04
## Changed
- there were two json_decode's which caused undesired behaviour

## [0.16.0] - 2020-06-04
## Added
- added a getAmount endpoint

## [0.15.0] - 2020-06-02
## Changes
- made the password an object field as Akamai recommends
- made the password object nullable to accomedate for light accountsgit 

## [0.14.4] - 2020-05-27
### Changes
- Fix: DisplayName now uses the actual displayname instead of the familyname.

## [0.14.3] - 2020-05-19
### Changes
- Several bugfixes, emailVerified can now be null by default (is necessary for Akamai.) & fixed some spelling errors.
- DateTimes that are not automaticly set are now nullable, because an Empty string will cause an error in akamai.
- Username is now nullable, since an empty string is not considered globally unique in akamai.

## [0.14.2] - 2020-05-19
### Added
- Set "isActive" to true by default if field is null.

## [0.14.1] - 2020-05-16
### Added
- Support for Lite accounts through a seperate LiteAccountDtoMapper and LiteAccountDto.

## [0.14.0] - 2020-05-16
### Added
- Mapped fields for "communities" to Account DTO.
- Field "emailVerified" to Account DTO.

### Changed
- Did some refactoring as a follow up to 0.13.0 to further keep responsibilities separate.
- Map "communitites" field to query string.

## [0.13.0] - 2020-05-13
### Added
- AkamaiOidcClient class, for an OIDC client, extends the AkamaiClient.

### Changed
- Reverted AkamaiClient class back to original state, not having OIDC implementation.
  The AkamaiClient can now be used as an admin client, with key & secret, while the OIDC functionality is seperated. 

## [0.12.0] - 2020-04-30
### Changed
- Made entity name in AccountDtoMapper based on ENV variable.

## [0.11.0] - 2020-04-30
### Added
- Compatibility for subscriptions in DTO.

## [0.10.0] - 2020-04-30
### Added
- Restored compatibility with the current state of the user entity scheme.

## [0.9.0] - 2020-04-29
### Added
- Functionality to retrieve the account belonging to the JSON Web Token.

## [0.8.1] - 2020-04-28
### Added
- Base number. Necessary for loading json web keys.

## [0.8] - 2020-04-24
### Added
- Validation for json web tokens.

## [0.7] - 2020-04-23
### Added
- it is now possible to inject the name of the user entity

## [0.6] - 2020-04-18
### Added
- Support for 'email' field in Akamai Scheme. (mandatory in creating an AccountDto).
- 'updateAccount' client endpoint, for updating a single account by AccountDto.

## [0.5] - 2020-04-16
### Added
- Support for Akamai accounts with the least possible parameters filled in. (minimal accounts)

### Changed
- Instead of Nullable basic types, the accountDto will now include an empty version of the basic type.
  example: instead of GivenName being null in the DTO, it will be an empty string now.

## [0.4.4] - 2020-04-16
### Changed
- Seperated the results check from the stat ok check in the client, so we can check seperately if the property results exists.

## [0.4.3] - 2020-04-15
### Added
- Made the Curl call more robust by expending the if statement when to give an exception.
  This fixes an error that no exception was thrown even when no results were found within the entity.find call

## [0.4.2] - 2020-03-23
### Added
- getByExternalId to ClientInterface.

## [0.4.1] - 2020-03-23
### Added
- getByExternalId endpoint.

## [0.3] - 2020-03-23
### Added
- Changelog
- Added option to delete accounts by it's UUID
