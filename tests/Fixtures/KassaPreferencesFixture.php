<?php

namespace Tests\BNNVARA\AkamaiClient\Fixtures;

use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\CarsTransport;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\CategoryCollection;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\ComputersTelecom;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\ElectronicsTelevision;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\FoodHealth;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\HolidayTravel;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\HousekeepingEnergy;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\LeisureGarden;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\MoneyLaw;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\ShopsEcommerce;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\WarrantyInsurance;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications\NotificationCollection;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications\NotifyNewAnswers;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications\NotifyNewQuestions;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications\NotifyNewReactions;

trait KassaPreferencesFixture
{
    public function getNotificationsThatAreAllSetToTrue(): NotificationCollection
    {
        $notifications = new NotificationCollection();

        $notifyNewAnswers = new NotifyNewAnswers();
        $notifyNewAnswers->setValue(true);

        $notifications->add($notifyNewAnswers);

        $notifyNewQuestions = new NotifyNewQuestions();
        $notifyNewQuestions->setValue(true);

        $notifications->add($notifyNewQuestions);

        $notifyNewReactions = new NotifyNewReactions();
        $notifyNewReactions->setValue(true);

        $notifications->add($notifyNewReactions);

        return $notifications;
    }

    public function getCategoriesThatAreAllSetToTrue(): CategoryCollection
    {
        $categories = new CategoryCollection();

        $carsTransport = new CarsTransport();
        $carsTransport->setValue(true);

        $computersTelecom = new ComputersTelecom();
        $computersTelecom->setValue(true);

        $electronicsTelevision = new ElectronicsTelevision();
        $electronicsTelevision->setValue(true);

        $housekeepingEnergy = new HousekeepingEnergy();
        $housekeepingEnergy->setValue(true);

        $moneyLaw = new MoneyLaw();
        $moneyLaw->setValue(true);

        $warrantyInsurance = new WarrantyInsurance();
        $warrantyInsurance->setValue(true);

        $holidayTravel = new HolidayTravel();
        $holidayTravel->setValue(true);

        $foodHealth = new FoodHealth();
        $foodHealth->setValue(true);

        $shopsEcommerce = new ShopsEcommerce();
        $shopsEcommerce->setValue(true);

        $leisureGarden = new LeisureGarden();
        $leisureGarden->setValue(true);

        $categories->add($carsTransport);
        $categories->add($computersTelecom);
        $categories->add($electronicsTelevision);
        $categories->add($housekeepingEnergy);
        $categories->add($moneyLaw);
        $categories->add($warrantyInsurance);
        $categories->add($holidayTravel);
        $categories->add($foodHealth);
        $categories->add($shopsEcommerce);
        $categories->add($leisureGarden);

        return $categories;
    }
}