<?php

namespace Tests\BNNVARA\AkamaiClient\Unit\Application\Mapper;

use BNNVARA\AkamaiClient\Application\Mapper\CommunitiesDtoMapper;
use BNNVARA\AkamaiClient\Domain\Communities\CommunitiesDto;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\Category;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\CategoryCollection;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\KassaDto;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications\Notification;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications\NotificationCollection;
use BNNVARA\AkamaiClient\Domain\Communities\Vroegevogels\VroegevogelsDto;
use BNNVARA\AkamaiClient\Domain\SingleFieldDtoInterface;
use PHPUnit\Framework\TestCase;
use stdClass;
use Tests\BNNVARA\AkamaiClient\Fixtures\KassaPreferencesFixture;

class CommunitiesDtoMapperTest extends TestCase
{
    use KassaPreferencesFixture;

    /** @test */
    public function itMapsACommunititesDtoToASingleAccountString(): void
    {
        $communities = $this->getCommunitiesDto();

        $mapper = new CommunitiesDtoMapper('userHL');

        $string = $mapper->mapSingleFieldToSingleAccountString($communities);

        $this->assertEquals(
            file_get_contents(__DIR__ . '/../../../Fixtures/singleAccountStringForCommunities.txt'),
            $string
        );
    }

    /** @test */
    public function itMapsASingleAccountJsonWithACommunitiesFieldValuesToACommunitiesDto(): void
    {
        $communities = new stdClass();

        $communities->kassa = new stdClass();
        $communities->kassa->showProfile = false;
        $communities->kassa->categories = new stdClass();
        $communities->kassa->categories->housekeeping_energy = true;
        $communities->kassa->categories->computers_telecom = true;
        $communities->kassa->categories->electronics_television = true;
        $communities->kassa->categories->leisure_garden = true;
        $communities->kassa->categories->warranty_insurance = true;
        $communities->kassa->categories->food_health = true;
        $communities->kassa->categories->holiday_travel = true;
        $communities->kassa->categories->cars_transport = true;
        $communities->kassa->categories->money_law = true;
        $communities->kassa->categories->shops_ecommerce = true;
        $communities->kassa->notifications = new stdClass();
        $communities->kassa->notifications->notifyNewAnswers = true;
        $communities->kassa->notifications->notifyNewQuestions = true;
        $communities->kassa->notifications->notifyNewReactions = true;
        $communities->vroegevogels = new stdClass();
        $communities->vroegevogels->showProfile = false;

        $account = new stdClass();
        $account->result = new stdClass();
        $account->result->uuid = '0a25ffb6-da73-4aba-8d1f-52f78e07c3dd';
        $account->result->communities = $communities;

        $singleAccountString = json_encode($account);

        $mapper = new CommunitiesDtoMapper(
            'userHL'
        );

        $dto = $mapper->mapSingleAccountJsonToSingleFieldDto($singleAccountString);

        $this->assertInstanceOf(CommunitiesDto::class, $dto);

        $this->assertEquals('0a25ffb6-da73-4aba-8d1f-52f78e07c3dd', $dto->getAccountId());
        $this->assertInstanceOf(KassaDto::class, $dto->getKassa());
        $this->assertInstanceOf(VroegevogelsDto::class, $dto->getVroegevogels());
        $this->assertInstanceOf(CategoryCollection::class, $dto->getKassa()->getCategories());
        $this->assertInstanceOf(NotificationCollection::class, $dto->getKassa()->getNotifications());

        $this->assertFalse($dto->getKassa()->showProfile());
        $this->assertFalse($dto->getVroegevogels()->showProfile());

        $categories = $dto->getKassa()->getCategories();
        $this->assertCount(10, $categories);

        foreach ($categories as $category) {
            $this->assertInstanceOf(Category::class, $category);
            $this->assertTrue($category->getValue());
        }

        $notifications = $dto->getKassa()->getNotifications();
        $this->assertCount(3, $notifications);

        foreach ($notifications as $notification) {
            $this->assertInstanceOf(Notification::class, $notification);
            $this->assertTrue($notification->getValue());
        }
    }

    /** @test */
    public function itMapsASingleAccountJsonWithoutCommunitiesFieldValuesToACommunitiesDtoWithDefaultValues(): void
    {
        $communities = new stdClass();

        $communities->kassa = null;
        $communities->vroegevogels = null;

        $account = new stdClass();
        $account->result = new stdClass();
        $account->result->uuid = '0a25ffb6-da73-4aba-8d1f-52f78e07c3dd';
        $account->result->communities = $communities;

        $singleAccountString = json_encode($account);

        $mapper = new CommunitiesDtoMapper(
            'userHL'
        );

        $dto = $mapper->mapSingleAccountJsonToSingleFieldDto($singleAccountString);

        $this->assertInstanceOf(CommunitiesDto::class, $dto);

        $this->assertEquals('0a25ffb6-da73-4aba-8d1f-52f78e07c3dd', $dto->getAccountId());
        $this->assertInstanceOf(KassaDto::class, $dto->getKassa());
        $this->assertInstanceOf(VroegevogelsDto::class, $dto->getVroegevogels());
        $this->assertInstanceOf(CategoryCollection::class, $dto->getKassa()->getCategories());
        $this->assertInstanceOf(NotificationCollection::class, $dto->getKassa()->getNotifications());

        $this->assertTrue($dto->getKassa()->showProfile());
        $this->assertTrue($dto->getVroegevogels()->showProfile());

        foreach ($dto->getKassa()->getCategories() as $category) {
            $this->assertInstanceOf(Category::class, $category);
            $this->assertFalse($category->getValue());
        }

        foreach ($dto->getKassa()->getNotifications() as $notification) {
            $this->assertInstanceOf(Notification::class, $notification);
            $this->assertFalse($notification->getValue());
        }
    }

    public function getCommunitiesDto(): SingleFieldDtoInterface
    {
        $categories = $this->getCategoriesThatAreAllSetToTrue();

        $notifications = $this->getNotificationsThatAreAllSetToTrue();

        $communities = new CommunitiesDto(
            '0a25ffb6-da73-4aba-8d1f-52f78e07c3dd',
            new KassaDto($categories, $notifications),
            new VroegevogelsDto()
        );

        return $communities;
    }
}