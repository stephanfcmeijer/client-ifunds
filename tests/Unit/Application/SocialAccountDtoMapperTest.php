<?php

namespace Tests\BNNVARA\AkamaiClient\Unit\Application;

use BNNVARA\AkamaiClient\Application\SocialAccountDtoMapper;
use BNNVARA\AkamaiClient\Domain\Exception\InvalidJsonException;
use BNNVARA\AkamaiClient\Domain\Exception\InvalidNumberOfAccountsException;
use BNNVARA\AkamaiClient\Domain\SocialAccountDto;
use DateTime;
use PHPUnit\Framework\TestCase;
use stdClass;

class SocialAccountDtoMapperTest extends TestCase
{
    /** @test */
    public function aSingleAccountApiResultIsMappedToAnAccountDto(): void
    {
        $singleAccountJson = file_get_contents(__DIR__ . '/../../Fixtures/singleAccountApiResult.json');
        $dtoMapper = new SocialAccountDtoMapper('user');

        $accountDto = $dtoMapper->mapSingleAccountJsonToSingleAccountDto($singleAccountJson);
        $this->verifyAccount($accountDto);
    }

    /** @test */
    public function aSingleAccountApiResultWithSomeNulledFieldsIsMappedToAnAccountDto(): void
    {
        $singleAccountJson = file_get_contents(
            __DIR__ . '/../../Fixtures/singleAccountApiResultWithSomeNulledFields.json'
        );

        $dtoMapper = new SocialAccountDtoMapper('user');

        $accountDto = $dtoMapper->mapSingleAccountJsonToSingleAccountDto($singleAccountJson);

        $this->assertFalse($accountDto->isBanned());
        $this->assertFalse($accountDto->isLockedOut());
        $this->assertTrue($accountDto->isActive());
    }

    /** @test */
    public function aMultipleAccountApiResultIsMappedToAnAccountDto(): void
    {
        $multipleAccountJson = file_get_contents(__DIR__ . '/../../Fixtures/multipleAccountSingleApiResult.json');
        $dtoMapper = new SocialAccountDtoMapper('user');

        $accountDto = $dtoMapper->mapMultipleAccountJsonToSingleAccountDto($multipleAccountJson);
        $this->verifyAccount($accountDto);
    }

    /** @test */
    public function aMinimalAccountIsMappedToAnAccountDto(): void
    {
        $singleAccountJson = file_get_contents(__DIR__ . '/../../Fixtures/minimalAccountJson.json');
        $dtoMapper = new SocialAccountDtoMapper('user');

        $accountDto = $dtoMapper->mapSingleAccountJsonToSingleAccountDto($singleAccountJson);
        $this->verifyMinimalAccount($accountDto);
    }

    /** @test */
    public function anExceptionIsThrownWhenToManyResultsAreFoundForASingleAccount(): void
    {
        $this->expectException(InvalidNumberOfAccountsException::class);

        $multipleAccountJson = file_get_contents(__DIR__ . '/../../Fixtures/multipleAccountMultipleApiResult.json');
        $dtoMapper = new SocialAccountDtoMapper('user');

        $dtoMapper->mapMultipleAccountJsonToSingleAccountDto($multipleAccountJson);
    }

    /** @test */
    public function anExceptionIsThrownWhenAnInvalidSingleJsonIsProvided(): void
    {
        $this->expectException(InvalidJsonException::class);
        $accountJson = file_get_contents(__DIR__ . '/../../Fixtures/invalidJson.json');

        $dtoMapper = new SocialAccountDtoMapper('user');
        $dtoMapper->mapSingleAccountJsonToSingleAccountDto($accountJson);
    }

    /** @test */
    public function anExceptionIsThrownWhenAnInvalidMultipleJsonIsProvided(): void
    {
        $this->expectException(InvalidJsonException::class);
        $accountJson = file_get_contents(__DIR__ . '/../../Fixtures/invalidJson.json');

        $dtoMapper = new SocialAccountDtoMapper('user');
        $dtoMapper->mapMultipleAccountJsonToSingleAccountDto($accountJson);
    }

    /**
     *  @test
     *  @dataProvider getAccountDtos
     */
    public function anAccountDtoIsMappedToAValidQueryString(SocialAccountDto $accountDto, string $expectedString): void
    {
        $dtoMapper = new SocialAccountDtoMapper('user');
        $urlParameters = $dtoMapper->mapAccountDtoToSingleAccountString($accountDto, true);

        $this->assertSame($expectedString, $urlParameters);
    }

    /**
     *  @test
     *  @dataProvider getAccountDtosWithoutUuids
     */
    public function anAccountDtoIsMappedToAValidQueryStringWithoutUuid(SocialAccountDto $accountDto, string $expectedString): void
    {
        $dtoMapper = new SocialAccountDtoMapper('user');
        $urlParameters = $dtoMapper->mapAccountDtoToSingleAccountString($accountDto, false);

        $this->assertSame($expectedString, $urlParameters);
    }

    public function getAccountDtos(): array
    {
        return [
            [
                $this->getAccountDtoWithoutPropertyIds(true),
                'uuid=195edeae-529f-47af-a23d-f910e173391a&type_name=user&include_record=true&attributes=%7B%22givenName%22%3A%22givenName%22%2C%22middleName%22%3A%22middleName%22%2C%22familyName%22%3A%22familyName%22%2C%22username%22%3A%22username%22%2C%22displayName%22%3A%22Qbrain%22%2C%22bio%22%3A%22Dit+is+mijn+Bio%22%2C%22firstContact%22%3A%22donation%22%2C%22emails%22%3A%5B%7B%22address%22%3A%22test%40example.com%22%2C%22preferred%22%3Atrue%7D%5D%2C%22email%22%3A%22myemail%40example.com%22%2C%22emailVerified%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%2C%22primaryAddress%22%3A%7B%22streetname%22%3A%22streetName%22%2C%22housenumber%22%3A%2210%22%2C%22housenumberAddition%22%3A%22a%22%2C%22city%22%3A%22city%22%2C%22zip%22%3A%22zip%22%2C%22country%22%3A%22NL%22%7D%2C%22registrationSource%22%3A%22kassa_nl%22%2C%22dateOfBirth%22%3A%7B%22birthYear%22%3A%221989%22%2C%22birthMonth%22%3A%2212%22%2C%22birthDay%22%3A%2211%22%7D%2C%22isActive%22%3Atrue%2C%22isLite%22%3Atrue%2C%22isRegistered%22%3Atrue%2C%22isLockedOut%22%3Atrue%2C%22isVerified%22%3Atrue%2C%22isBanned%22%3Atrue%2C%22contactNumber%22%3A%22xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxy%22%2C%22relationNumber%22%3A%2212345678%22%2C%22externalId%22%3A%22xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx%22%2C%22gender%22%3A%22Male%22%2C%22consents%22%3A%7B%22tracking%22%3A%7B%22granted%22%3Afalse%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%7D%2C%22marketing%22%3A%7B%22granted%22%3Atrue%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%7D%7D%2C%22photos%22%3A%5B%7B%22type%22%3A%22avatar%22%2C%22value%22%3A%22https%3A%5C%2F%5C%2Fwww.example.com%5C%2Fa%22%7D%5D%2C%22phones%22%3A%5B%7B%22number%22%3A%220600000000%22%7D%2C%7B%22number%22%3A%220611111111%22%7D%5D%2C%22legalAcceptances%22%3A%5B%7B%22dateAccepted%22%3A%222010-12-12+12%3A12%3A12+%2B0100%22%2C%22hasAcceptedTerms%22%3Atrue%2C%22legalAcceptanceId%22%3A%22terms%22%7D%5D%2C%22subscriptions%22%3A%7B%22newsletters%22%3A%5B%7B%22doubleOptInStatus%22%3A%22Confirmed%22%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%2C%22name%22%3A%22DWDD%22%7D%5D%7D%2C%22communities%22%3A%7B%22vroegevogels%22%3A%7B%7D%2C%22kassa%22%3A%7B%7D%7D%7D'
            ],
            [
                $this->getAccountDtoWithoutPropertyIds(true),
                'uuid=195edeae-529f-47af-a23d-f910e173391a&type_name=user&include_record=true&attributes=%7B%22givenName%22%3A%22givenName%22%2C%22middleName%22%3A%22middleName%22%2C%22familyName%22%3A%22familyName%22%2C%22username%22%3A%22username%22%2C%22displayName%22%3A%22Qbrain%22%2C%22bio%22%3A%22Dit+is+mijn+Bio%22%2C%22firstContact%22%3A%22donation%22%2C%22emails%22%3A%5B%7B%22address%22%3A%22test%40example.com%22%2C%22preferred%22%3Atrue%7D%5D%2C%22email%22%3A%22myemail%40example.com%22%2C%22emailVerified%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%2C%22primaryAddress%22%3A%7B%22streetname%22%3A%22streetName%22%2C%22housenumber%22%3A%2210%22%2C%22housenumberAddition%22%3A%22a%22%2C%22city%22%3A%22city%22%2C%22zip%22%3A%22zip%22%2C%22country%22%3A%22NL%22%7D%2C%22registrationSource%22%3A%22kassa_nl%22%2C%22dateOfBirth%22%3A%7B%22birthYear%22%3A%221989%22%2C%22birthMonth%22%3A%2212%22%2C%22birthDay%22%3A%2211%22%7D%2C%22isActive%22%3Atrue%2C%22isLite%22%3Atrue%2C%22isRegistered%22%3Atrue%2C%22isLockedOut%22%3Atrue%2C%22isVerified%22%3Atrue%2C%22isBanned%22%3Atrue%2C%22contactNumber%22%3A%22xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxy%22%2C%22relationNumber%22%3A%2212345678%22%2C%22externalId%22%3A%22xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx%22%2C%22gender%22%3A%22Male%22%2C%22consents%22%3A%7B%22tracking%22%3A%7B%22granted%22%3Afalse%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%7D%2C%22marketing%22%3A%7B%22granted%22%3Atrue%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%7D%7D%2C%22photos%22%3A%5B%7B%22type%22%3A%22avatar%22%2C%22value%22%3A%22https%3A%5C%2F%5C%2Fwww.example.com%5C%2Fa%22%7D%5D%2C%22phones%22%3A%5B%7B%22number%22%3A%220600000000%22%7D%2C%7B%22number%22%3A%220611111111%22%7D%5D%2C%22legalAcceptances%22%3A%5B%7B%22dateAccepted%22%3A%222010-12-12+12%3A12%3A12+%2B0100%22%2C%22hasAcceptedTerms%22%3Atrue%2C%22legalAcceptanceId%22%3A%22terms%22%7D%5D%2C%22subscriptions%22%3A%7B%22newsletters%22%3A%5B%7B%22doubleOptInStatus%22%3A%22Confirmed%22%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%2C%22name%22%3A%22DWDD%22%7D%5D%7D%2C%22communities%22%3A%7B%22vroegevogels%22%3A%7B%7D%2C%22kassa%22%3A%7B%7D%7D%7D'
            ],
            [
                $this->getAccountDtoWithoutPropertyIds(false),
                'uuid=195edeae-529f-47af-a23d-f910e173391a&type_name=user&include_record=true&attributes=%7B%22givenName%22%3A%22givenName%22%2C%22middleName%22%3A%22middleName%22%2C%22familyName%22%3A%22familyName%22%2C%22username%22%3A%22username%22%2C%22displayName%22%3A%22Qbrain%22%2C%22bio%22%3A%22Dit+is+mijn+Bio%22%2C%22firstContact%22%3A%22donation%22%2C%22emails%22%3A%5B%7B%22address%22%3A%22test%40example.com%22%2C%22preferred%22%3Atrue%7D%5D%2C%22email%22%3A%22myemail%40example.com%22%2C%22emailVerified%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%2C%22primaryAddress%22%3A%7B%22streetname%22%3A%22streetName%22%2C%22housenumber%22%3A%2210%22%2C%22housenumberAddition%22%3A%22a%22%2C%22city%22%3A%22city%22%2C%22zip%22%3A%22zip%22%2C%22country%22%3A%22NL%22%7D%2C%22registrationSource%22%3A%22kassa_nl%22%2C%22dateOfBirth%22%3A%7B%22birthYear%22%3A%221989%22%2C%22birthMonth%22%3A%2212%22%2C%22birthDay%22%3A%2211%22%7D%2C%22isActive%22%3Atrue%2C%22isLite%22%3Atrue%2C%22isRegistered%22%3Atrue%2C%22isLockedOut%22%3Atrue%2C%22isVerified%22%3Atrue%2C%22isBanned%22%3Atrue%2C%22contactNumber%22%3A%22xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxy%22%2C%22relationNumber%22%3A%2212345678%22%2C%22externalId%22%3A%22xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx%22%2C%22gender%22%3A%22Male%22%2C%22consents%22%3A%7B%22tracking%22%3A%7B%22granted%22%3Afalse%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%7D%2C%22marketing%22%3A%7B%22granted%22%3Atrue%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%7D%7D%2C%22photos%22%3A%5B%7B%22type%22%3A%22avatar%22%2C%22value%22%3A%22https%3A%5C%2F%5C%2Fwww.example.com%5C%2Fa%22%7D%5D%2C%22phones%22%3A%5B%7B%22number%22%3A%220600000000%22%7D%2C%7B%22number%22%3A%220611111111%22%7D%5D%2C%22legalAcceptances%22%3A%5B%7B%22dateAccepted%22%3A%222010-12-12+12%3A12%3A12+%2B0100%22%2C%22hasAcceptedTerms%22%3Atrue%2C%22legalAcceptanceId%22%3A%22terms%22%7D%5D%2C%22subscriptions%22%3A%7B%22newsletters%22%3A%5B%7B%22doubleOptInStatus%22%3A%22Confirmed%22%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%2C%22name%22%3A%22DWDD%22%7D%5D%7D%2C%22communities%22%3A%7B%22vroegevogels%22%3A%7B%22showProfile%22%3Atrue%7D%2C%22kassa%22%3A%7B%22showProfile%22%3Atrue%2C%22categories%22%3A%7B%22housekeeping_energy%22%3Atrue%2C%22electronics_television%22%3Atrue%2C%22leisure_garden%22%3Atrue%2C%22warranty_insurance%22%3Atrue%2C%22food_health%22%3Atrue%2C%22holiday_travel%22%3Atrue%2C%22cars_transport%22%3Atrue%2C%22money_law%22%3Atrue%2C%22shops_ecommerce%22%3Atrue%7D%2C%22notifications%22%3A%7B%22notifyNewAnswers%22%3Atrue%2C%22notifyNewQuestions%22%3Atrue%2C%22notifyNewReactions%22%3Atrue%7D%7D%7D%7D'
            ],
            [
                $this->getAccountDtoWithPropertyIds(false),
                'uuid=195edeae-529f-47af-a23d-f910e173391a&type_name=user&include_record=true&attributes=%7B%22givenName%22%3A%22givenName%22%2C%22middleName%22%3A%22middleName%22%2C%22familyName%22%3A%22familyName%22%2C%22username%22%3A%22username%22%2C%22displayName%22%3A%22Qbrain%22%2C%22bio%22%3A%22Dit+is+mijn+Bio%22%2C%22firstContact%22%3A%22donation%22%2C%22emails%22%3A%5B%7B%22id%22%3A%221%22%2C%22address%22%3A%22test%40example.com%22%2C%22preferred%22%3Atrue%7D%5D%2C%22email%22%3A%22myemail%40example.com%22%2C%22emailVerified%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%2C%22primaryAddress%22%3A%7B%22streetname%22%3A%22streetName%22%2C%22housenumber%22%3A%2210%22%2C%22housenumberAddition%22%3A%22a%22%2C%22city%22%3A%22city%22%2C%22zip%22%3A%22zip%22%2C%22country%22%3A%22NL%22%7D%2C%22registrationSource%22%3A%22kassa_nl%22%2C%22dateOfBirth%22%3A%7B%22birthYear%22%3A%221989%22%2C%22birthMonth%22%3A%2212%22%2C%22birthDay%22%3A%2211%22%7D%2C%22isActive%22%3Atrue%2C%22isLite%22%3Atrue%2C%22isRegistered%22%3Atrue%2C%22isLockedOut%22%3Atrue%2C%22isVerified%22%3Atrue%2C%22isBanned%22%3Atrue%2C%22contactNumber%22%3A%22xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxy%22%2C%22relationNumber%22%3A%2212345678%22%2C%22externalId%22%3A%22xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx%22%2C%22gender%22%3A%22Male%22%2C%22consents%22%3A%7B%22tracking%22%3A%7B%22granted%22%3Afalse%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%7D%2C%22marketing%22%3A%7B%22granted%22%3Atrue%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%7D%7D%2C%22photos%22%3A%5B%7B%22id%22%3A%221%22%2C%22type%22%3A%22avatar%22%2C%22value%22%3A%22https%3A%5C%2F%5C%2Fwww.example.com%5C%2Fa%22%7D%5D%2C%22phones%22%3A%5B%7B%22id%22%3A%221%22%2C%22number%22%3A%220600000000%22%7D%2C%7B%22id%22%3A%222%22%2C%22number%22%3A%220611111111%22%7D%5D%2C%22legalAcceptances%22%3A%5B%7B%22id%22%3A%221%22%2C%22dateAccepted%22%3A%222010-12-12+12%3A12%3A12+%2B0100%22%2C%22hasAcceptedTerms%22%3Atrue%2C%22legalAcceptanceId%22%3A%22terms%22%7D%5D%2C%22subscriptions%22%3A%7B%22newsletters%22%3A%5B%7B%22id%22%3A%222563%22%2C%22doubleOptInStatus%22%3A%22Confirmed%22%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%2C%22name%22%3A%22DWDD%22%7D%5D%7D%2C%22communities%22%3A%7B%22vroegevogels%22%3A%7B%22showProfile%22%3Atrue%7D%2C%22kassa%22%3A%7B%22showProfile%22%3Atrue%2C%22categories%22%3A%7B%22housekeeping_energy%22%3Atrue%2C%22electronics_television%22%3Atrue%2C%22leisure_garden%22%3Atrue%2C%22warranty_insurance%22%3Atrue%2C%22food_health%22%3Atrue%2C%22holiday_travel%22%3Atrue%2C%22cars_transport%22%3Atrue%2C%22money_law%22%3Atrue%2C%22shops_ecommerce%22%3Atrue%7D%2C%22notifications%22%3A%7B%22notifyNewAnswers%22%3Atrue%2C%22notifyNewQuestions%22%3Atrue%2C%22notifyNewReactions%22%3Atrue%7D%7D%7D%7D'
            ]
        ];
    }

    public function getAccountDtosWithoutUuids(): array
    {
        return [
            [
                $this->getAccountDtoWithoutPropertyIds(false),
                'type_name=user&include_record=true&attributes=%7B%22givenName%22%3A%22givenName%22%2C%22middleName%22%3A%22middleName%22%2C%22familyName%22%3A%22familyName%22%2C%22username%22%3A%22username%22%2C%22displayName%22%3A%22Qbrain%22%2C%22bio%22%3A%22Dit+is+mijn+Bio%22%2C%22firstContact%22%3A%22donation%22%2C%22emails%22%3A%5B%7B%22address%22%3A%22test%40example.com%22%2C%22preferred%22%3Atrue%7D%5D%2C%22email%22%3A%22myemail%40example.com%22%2C%22emailVerified%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%2C%22primaryAddress%22%3A%7B%22streetname%22%3A%22streetName%22%2C%22housenumber%22%3A%2210%22%2C%22housenumberAddition%22%3A%22a%22%2C%22city%22%3A%22city%22%2C%22zip%22%3A%22zip%22%2C%22country%22%3A%22NL%22%7D%2C%22registrationSource%22%3A%22kassa_nl%22%2C%22dateOfBirth%22%3A%7B%22birthYear%22%3A%221989%22%2C%22birthMonth%22%3A%2212%22%2C%22birthDay%22%3A%2211%22%7D%2C%22isActive%22%3Atrue%2C%22isLite%22%3Atrue%2C%22isRegistered%22%3Atrue%2C%22isLockedOut%22%3Atrue%2C%22isVerified%22%3Atrue%2C%22isBanned%22%3Atrue%2C%22contactNumber%22%3A%22xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxy%22%2C%22relationNumber%22%3A%2212345678%22%2C%22externalId%22%3A%22xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx%22%2C%22gender%22%3A%22Male%22%2C%22consents%22%3A%7B%22tracking%22%3A%7B%22granted%22%3Afalse%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%7D%2C%22marketing%22%3A%7B%22granted%22%3Atrue%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%7D%7D%2C%22photos%22%3A%5B%7B%22type%22%3A%22avatar%22%2C%22value%22%3A%22https%3A%5C%2F%5C%2Fwww.example.com%5C%2Fa%22%7D%5D%2C%22phones%22%3A%5B%7B%22number%22%3A%220600000000%22%7D%2C%7B%22number%22%3A%220611111111%22%7D%5D%2C%22legalAcceptances%22%3A%5B%7B%22dateAccepted%22%3A%222010-12-12+12%3A12%3A12+%2B0100%22%2C%22hasAcceptedTerms%22%3Atrue%2C%22legalAcceptanceId%22%3A%22terms%22%7D%5D%2C%22subscriptions%22%3A%7B%22newsletters%22%3A%5B%7B%22doubleOptInStatus%22%3A%22Confirmed%22%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%2C%22name%22%3A%22DWDD%22%7D%5D%7D%2C%22communities%22%3A%7B%22vroegevogels%22%3A%7B%22showProfile%22%3Atrue%7D%2C%22kassa%22%3A%7B%22showProfile%22%3Atrue%2C%22categories%22%3A%7B%22housekeeping_energy%22%3Atrue%2C%22electronics_television%22%3Atrue%2C%22leisure_garden%22%3Atrue%2C%22warranty_insurance%22%3Atrue%2C%22food_health%22%3Atrue%2C%22holiday_travel%22%3Atrue%2C%22cars_transport%22%3Atrue%2C%22money_law%22%3Atrue%2C%22shops_ecommerce%22%3Atrue%7D%2C%22notifications%22%3A%7B%22notifyNewAnswers%22%3Atrue%2C%22notifyNewQuestions%22%3Atrue%2C%22notifyNewReactions%22%3Atrue%7D%7D%7D%7D'
            ],
            [
                $this->getAccountDtoWithPropertyIds(true),
                'type_name=user&include_record=true&attributes=%7B%22givenName%22%3A%22givenName%22%2C%22middleName%22%3A%22middleName%22%2C%22familyName%22%3A%22familyName%22%2C%22username%22%3A%22username%22%2C%22displayName%22%3A%22Qbrain%22%2C%22bio%22%3A%22Dit+is+mijn+Bio%22%2C%22firstContact%22%3A%22donation%22%2C%22emails%22%3A%5B%7B%22id%22%3A%221%22%2C%22address%22%3A%22test%40example.com%22%2C%22preferred%22%3Atrue%7D%5D%2C%22email%22%3A%22myemail%40example.com%22%2C%22emailVerified%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%2C%22primaryAddress%22%3A%7B%22streetname%22%3A%22streetName%22%2C%22housenumber%22%3A%2210%22%2C%22housenumberAddition%22%3A%22a%22%2C%22city%22%3A%22city%22%2C%22zip%22%3A%22zip%22%2C%22country%22%3A%22NL%22%7D%2C%22registrationSource%22%3A%22kassa_nl%22%2C%22dateOfBirth%22%3A%7B%22birthYear%22%3A%221989%22%2C%22birthMonth%22%3A%2212%22%2C%22birthDay%22%3A%2211%22%7D%2C%22isActive%22%3Atrue%2C%22isLite%22%3Atrue%2C%22isRegistered%22%3Atrue%2C%22isLockedOut%22%3Atrue%2C%22isVerified%22%3Atrue%2C%22isBanned%22%3Atrue%2C%22contactNumber%22%3A%22xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxy%22%2C%22relationNumber%22%3A%2212345678%22%2C%22externalId%22%3A%22xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx%22%2C%22gender%22%3A%22Male%22%2C%22consents%22%3A%7B%22tracking%22%3A%7B%22granted%22%3Afalse%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%7D%2C%22marketing%22%3A%7B%22granted%22%3Atrue%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%7D%7D%2C%22photos%22%3A%5B%7B%22id%22%3A%221%22%2C%22type%22%3A%22avatar%22%2C%22value%22%3A%22https%3A%5C%2F%5C%2Fwww.example.com%5C%2Fa%22%7D%5D%2C%22phones%22%3A%5B%7B%22id%22%3A%221%22%2C%22number%22%3A%220600000000%22%7D%2C%7B%22id%22%3A%222%22%2C%22number%22%3A%220611111111%22%7D%5D%2C%22legalAcceptances%22%3A%5B%7B%22id%22%3A%221%22%2C%22dateAccepted%22%3A%222010-12-12+12%3A12%3A12+%2B0100%22%2C%22hasAcceptedTerms%22%3Atrue%2C%22legalAcceptanceId%22%3A%22terms%22%7D%5D%2C%22subscriptions%22%3A%7B%22newsletters%22%3A%5B%7B%22id%22%3A%222563%22%2C%22doubleOptInStatus%22%3A%22Confirmed%22%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%2C%22name%22%3A%22DWDD%22%7D%5D%7D%2C%22communities%22%3A%7B%22vroegevogels%22%3A%7B%7D%2C%22kassa%22%3A%7B%7D%7D%7D'
            ]
        ];
    }

    private function verifyAccount(SocialAccountDto $accountDto): void
    {
        $this->assertSame('2df8ad57-fffd-4f02-882c-2821bd849bfc', $accountDto->getAccountId());
        $this->assertSame('Danny', $accountDto->getGivenName());
        $this->assertSame('de', $accountDto->getMiddleName());
        $this->assertSame('Eerens', $accountDto->getFamilyName());
        $this->assertSame('Qbrain', $accountDto->getDisplayName());
        $this->assertSame('Qbrain', $accountDto->getUsername());
        $this->assertSame('dit is mijn bio', $accountDto->getBio());
        $this->assertSame('myemail@example.com', $accountDto->getEmail());
        $this->assertSame('2020-03-29 13:36:39.990263 +0000', $accountDto->getEmailVerified());
        $this->assertTrue($accountDto->isVerified());
        $this->assertTrue($accountDto->isRegistered());
        $this->assertTrue($accountDto->isActive());
        $this->assertFalse($accountDto->isLockedOut());
        $this->assertFalse($accountDto->isLite());
        $this->assertFalse($accountDto->isBanned());
        $this->assertSame('danny.eerens@bnnvara.nl', $accountDto->getEmails()[0]->address);
        $this->assertTrue($accountDto->getEmails()[0]->preferred);
        $this->assertSame('donation', $accountDto->getFirstContact());
        $this->assertSame('verzonnenAdres', $accountDto->getPrimaryAddressStreetName());
        $this->assertSame('111', $accountDto->getPrimaryAddressHouseNumber());
        $this->assertSame('a', $accountDto->getPrimaryAddressHouseNumberAddition());
        $this->assertSame('NL', $accountDto->getPrimaryAddressCountry());
        $this->assertSame('verzonnenStad', $accountDto->getPrimaryAddressCity());
        $this->assertSame('0000ZZ', $accountDto->getPrimaryAddressZip());
        $this->assertTrue($accountDto->getHasMarketingConsent());
        $this->assertNotNull($accountDto->getMarketingConsentUpdatedDateTime());
        $this->assertFalse($accountDto->getHasTrackingConsent());
        $this->assertNotNull($accountDto->getTrackingConsentUpdatedDateTime());
        $this->assertSame('1989', $accountDto->getBirthYear());
        $this->assertSame('12', $accountDto->getBirthMonth());
        $this->assertSame('11', $accountDto->getBirthDay());
        $this->assertSame('Kassa_nl', $accountDto->getRegistrationSource());
        $this->assertSame('xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx', $accountDto->getContactNumber());
        $this->assertSame('hashed-password', $accountDto->getPassword()->value);
        $this->assertSame('encryption-type', $accountDto->getPassword()->type);
        $this->assertSame('yyyyyyyy-yyyy-yyyy-yyyy-yyyyyyyyyyyy', $accountDto->getExternalId());
        $this->assertSame('123456', $accountDto->getRelationNumber());
        $this->assertSame('0600000000', $accountDto->getPhones()[0]->number);
        $this->assertSame('0611111111', $accountDto->getPhones()[1]->number);
        $this->assertSame('https://www.example.com', $accountDto->getPhotos()[0]->value);
        $this->assertSame('avatar', $accountDto->getPhotos()[0]->type);
        $this->assertSame('terms', $accountDto->getLegalAcceptances()[0]->legalAcceptanceId);
        $this->assertSame('Male', $accountDto->getGender());
        $this->assertTrue($accountDto->getLegalAcceptances()[0]->hasAcceptedTerms);
        $this->assertTrue($accountDto->getEmails()[0]->preferred);
        $this->assertNotNull($accountDto->getCreatedDateTime());
        $this->assertNotNull($accountDto->getLastUpdated());
        $this->assertSame('Confirmed', $accountDto->getSubscriptions()->newsletters[0]->doubleOptInStatus);
        $this->assertNotNull($accountDto->getSubscriptions()->newsletters[0]->updated);
        $this->assertSame('DWDD', $accountDto->getSubscriptions()->newsletters[0]->name);
        $communities = $accountDto->getCommunities();
        $this->assertIsObject($communities);
        $this->assertTrue($communities->vroegevogels->showProfile);
        $this->assertTrue($communities->kassa->showProfile);
        $this->assertTrue($communities->kassa->categories->housekeeping_energy);
        $this->assertTrue($communities->kassa->categories->electronics_television);
        $this->assertTrue($communities->kassa->categories->leisure_garden);
        $this->assertTrue($communities->kassa->categories->warranty_insurance);
        $this->assertTrue($communities->kassa->categories->food_health);
        $this->assertTrue($communities->kassa->categories->holiday_travel);
        $this->assertTrue($communities->kassa->categories->cars_transport);
        $this->assertTrue($communities->kassa->categories->money_law);
        $this->assertTrue($communities->kassa->categories->shops_ecommerce);
        $this->assertTrue($communities->kassa->notifications->notifyNewAnswers);
        $this->assertTrue($communities->kassa->notifications->notifyNewQuestions);
        $this->assertTrue($communities->kassa->notifications->notifyNewReactions);
    }

    private function verifyMinimalAccount(SocialAccountDto $accountDto): void
    {
        $this->assertSame('195edeae-529f-47af-a23d-f910e173391a', $accountDto->getAccountId());
        $this->assertSame('', $accountDto->getGivenName());
        $this->assertSame('', $accountDto->getMiddleName());
        $this->assertSame('', $accountDto->getFamilyName());
        $this->assertSame('', $accountDto->getDisplayName());
        $this->assertNull($accountDto->getUsername());
        $this->assertSame('', $accountDto->getBio());
        $this->assertSame('', $accountDto->getEmail());
        $this->assertNull($accountDto->getEmailVerified());
        $this->assertFalse($accountDto->isVerified());
        $this->assertFalse($accountDto->isRegistered());
        $this->assertFalse($accountDto->isActive());
        $this->assertFalse($accountDto->isLockedOut());
        $this->assertTrue($accountDto->isLite());
        $this->assertFalse($accountDto->isBanned());
        $this->assertIsArray($accountDto->getEmails());
        $this->assertSame('', $accountDto->getFirstContact());
        $this->assertSame('', $accountDto->getPrimaryAddressStreetName());
        $this->assertSame('', $accountDto->getPrimaryAddressHouseNumber());
        $this->assertSame('', $accountDto->getPrimaryAddressHouseNumberAddition());
        $this->assertSame('', $accountDto->getPrimaryAddressCountry());
        $this->assertSame('', $accountDto->getPrimaryAddressCity());
        $this->assertSame('', $accountDto->getPrimaryAddressZip());
        $this->assertFalse($accountDto->getHasMarketingConsent());
        $this->assertNull($accountDto->getMarketingConsentUpdatedDateTime());
        $this->assertFalse($accountDto->getHasTrackingConsent());
        $this->assertNull($accountDto->getTrackingConsentUpdatedDateTime());
        $this->assertSame('', $accountDto->getBirthYear());
        $this->assertSame('', $accountDto->getBirthMonth());
        $this->assertSame('', $accountDto->getBirthDay());
        $this->assertSame('', $accountDto->getRegistrationSource());
        $this->assertSame('', $accountDto->getContactNumber());
        $this->assertNull($accountDto->getPassword());
        $this->assertSame('', $accountDto->getExternalId());
        $this->assertSame('', $accountDto->getRelationNumber());
        $this->assertIsArray($accountDto->getPhones());
        $this->assertIsArray($accountDto->getPhotos());
        $this->assertSame('', $accountDto->getGender());
        $this->assertIsArray($accountDto->getLegalAcceptances());
        $this->assertIsString($accountDto->getCreatedDateTime());
        $this->assertIsString($accountDto->getLastUpdated());
        $this->assertIsArray($accountDto->getSubscriptions()->newsletters);
    }

    private function getAccountDtoWithoutPropertyIds(bool $nullCommunities): SocialAccountDto
    {
        $datetime = new DateTime('2010-12-12 12:12:12');
        $formattedDateTime = $datetime->format('c');

        $emails = new stdClass();
        $emails->preferred = true;
        $emails->address = 'test@example.com';

        $photo1 = new stdClass();
        $photo1->value = 'https://www.example.com/a';
        $photo1->type = 'avatar';

        $phone1 = new stdClass();
        $phone1->number = '0600000000';

        $phone2 = new stdClass();
        $phone2->number = '0611111111';

        $legalAcceptance1 = new stdClass();
        $legalAcceptance1->hasAcceptedTerms = true;
        $legalAcceptance1->legalAcceptanceId = 'terms';
        $legalAcceptance1->dateAccepted = '2010-12-12 12:12:12 +0100';

        $subscription1 = new stdClass();
        $subscription1->doubleOptInStatus = "Confirmed";
        $subscription1->updated = $datetime->format('c');
        $subscription1->name = "DWDD";

        $subscriptions = new stdClass();
        $subscriptions->newsletters = [$subscription1];

        $communitites = $this->getCommunitiesObject($nullCommunities);

        return new SocialAccountDto(
            '195edeae-529f-47af-a23d-f910e173391a',
            'givenName',
            'middleName',
            'familyName',
            'username',
            'Qbrain',
            'Dit is mijn Bio',
            true,
            true,
            true,
            true,
            true,
            true,
            [$emails],
            'myemail@example.com',
            $formattedDateTime,
            $formattedDateTime,
            'donation',
            'streetName',
            '10',
            'a',
            'NL',
            'city',
            'zip',
            true,
            $formattedDateTime,
            false,
            $formattedDateTime,
            '1989',
            '12',
            '11',
            $formattedDateTime,
            'kassa_nl',
            null,
            [$phone1, $phone2],
            [$photo1],
            'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
            'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxy',
            '12345678',
            [$legalAcceptance1],
            'Male',
            $subscriptions,
            $communitites
        );
    }

    private function getCommunitiesObject(bool $null): object
    {
        $communities = new stdClass();

        if ($null === false) {
            $communities->vroegevogels = new stdClass();
            $communities->kassa = new stdClass();
            $communities->vroegevogels->showProfile = true;
            $communities->kassa->showProfile = true;
            $communities->kassa->categories = new stdClass();
            $communities->kassa->categories->housekeeping_energy = true;
            $communities->kassa->categories->electronics_television = true;
            $communities->kassa->categories->leisure_garden = true;
            $communities->kassa->categories->warranty_insurance = true;
            $communities->kassa->categories->food_health = true;
            $communities->kassa->categories->holiday_travel = true;
            $communities->kassa->categories->cars_transport = true;
            $communities->kassa->categories->money_law = true;
            $communities->kassa->categories->shops_ecommerce = true;
            $communities->kassa->notifications = new stdClass();
            $communities->kassa->notifications->notifyNewAnswers = true;
            $communities->kassa->notifications->notifyNewQuestions = true;
            $communities->kassa->notifications->notifyNewReactions = true;
        } else {
            $communities->vroegevogels = new stdClass();
            $communities->kassa = new stdClass();
        }

        return $communities;
    }

    private function getAccountDtoWithPropertyIds(bool $nullCommunities): SocialAccountDto
    {
        $datetime = new DateTime('2010-12-12 12:12:12');
        $formattedDateTime = $datetime->format('c');

        $emails = new stdClass();
        $emails->id = '1';
        $emails->preferred = true;
        $emails->address = 'test@example.com';

        $photo1 = new stdClass();
        $photo1->id = '1';
        $photo1->value = 'https://www.example.com/a';
        $photo1->type = 'avatar';

        $phone1 = new stdClass();
        $phone1->id = '1';
        $phone1->number = '0600000000';

        $phone2 = new stdClass();
        $phone2->id = '2';
        $phone2->number = '0611111111';

        $legalAcceptance1 = new stdClass();
        $legalAcceptance1->id = '1';
        $legalAcceptance1->hasAcceptedTerms = true;
        $legalAcceptance1->legalAcceptanceId = 'terms';
        $legalAcceptance1->dateAccepted = '2010-12-12 12:12:12 +0100';

        $subscription1 = new stdClass();
        $subscription1->id = '2563';
        $subscription1->doubleOptInStatus = 'Confirmed';
        $subscription1->updated = $datetime->format('c');
        $subscription1->name = 'DWDD';

        $subscriptions = new stdClass();
        $subscriptions->newsletters = [$subscription1];

        $communitites = $this->getCommunitiesObject($nullCommunities);

        $password = new \stdClass();
        $password->value = 'hashed-password';
        $password->type = 'encryption-type';

        return new SocialAccountDto(
            '195edeae-529f-47af-a23d-f910e173391a',
            'givenName',
            'middleName',
            'familyName',
            'username',
            'Qbrain',
            'Dit is mijn Bio',
            true,
            true,
            true,
            true,
            true,
            true,
            [$emails],
            'myemail@example.com',
            $formattedDateTime,
            $formattedDateTime,
            'donation',
            'streetName',
            '10',
            'a',
            'NL',
            'city',
            'zip',
            true,
            $formattedDateTime,
            false,
            $formattedDateTime,
            '1989',
            '12',
            '11',
            $formattedDateTime,
            'kassa_nl',
            $password,
            [$phone1, $phone2],
            [$photo1],
            'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
            'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxy',
            '12345678',
            [$legalAcceptance1],
            'Male',
            $subscriptions,
            $communitites
        );
    }
}