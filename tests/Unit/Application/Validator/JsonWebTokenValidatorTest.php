<?php

namespace Tests\BNNVARA\AkamaiClient\Unit\Application\Validator;

use BNNVARA\AkamaiClient\Application\Decoder\DecoderInterface;
use BNNVARA\AkamaiClient\Application\Validator\Exception\InvalidJsonWebTokenException;
use BNNVARA\AkamaiClient\Application\Validator\JsonWebTokenValidator;
use BNNVARA\AkamaiClient\Domain\JsonWebKeyDto;
use BNNVARA\AkamaiClient\Domain\JsonWebKeyDtoCollection;
use Exception;
use phpseclib\Crypt\RSA;
use PHPUnit\Framework\TestCase;

class JsonWebTokenValidatorTest extends TestCase
{
    /** @test */
    public function jsonWebTokenIsConsideredValidWhenValidatedAgainstJsonWebKeys(): void
    {
        $validator = new JsonWebTokenValidator(
            $this->getJsonWebKeyDtoCollectionWithMultipleKeys(),
            $this->getBase64Decoder(),
            $this->getSanitizedBase64Decoder()
        );

        $jsonWebToken = <<<TOKEN
eyJhbGciOiJSUzI1NiIsImtpZCI6IjNjNTE0MzYyN2Y3NDFiOGVmM2ZhOWFmNWExODdhZGM3OGQwMDQ1MzgiLCJ0eXAiOiJKV1QifQ.pay_load.c2lnbmF0dXJl
TOKEN;

        $this->assertTrue($validator->validate($jsonWebToken));
    }

    /** @test */
    public function invalidJsonWebTokenIsRejected(): void
    {
        $validator = new JsonWebTokenValidator(
            $this->getJsonWebKeyDtoCollectionWithDtoThatReturnsFalseUponVerification(),
            $this->getBase64Decoder(),
            $this->getSanitizedBase64Decoder()
        );

        $jsonWebToken = <<<TOKEN
eyJhbGciOiJSUzI1NiIsImtpZCI6IjNjNTE0MzYyN2Y3NDFiOGVmM2ZhOWFmNWExODdhZGM3OGQwMDQ1MzgiLCJ0eXAiOiJKV1QifQ.payload.c2lnbmF0dXJl
TOKEN;

        $this->assertFalse($validator->validate($jsonWebToken));
    }

    /** @test */
    public function jsonWebTokenWithAnInvalidSignatureIsRejected(): void
    {
        $validator = new JsonWebTokenValidator(
            $this->getJsonWebKeyDtoCollectionWithDtoThatThrowsAnExceptionBecauseTheSignatureIsInvalid(),
            $this->getBase64Decoder(),
            $this->getSanitizedBase64DecoderThatReturnsAnInvalidSignature()
        );

        $jsonWebToken = <<<TOKEN
eyJhbGciOiJSUzI1NiIsImtpZCI6IjNjNTE0MzYyN2Y3NDFiOGVmM2ZhOWFmNWExODdhZGM3OGQwMDQ1MzgiLCJ0eXAiOiJKV1QifQ.payload.aW52YWxpZF9zaWduYXR1cmU
TOKEN;

        $this->assertFalse($validator->validate($jsonWebToken));
    }

    /**
     * @test
     * @dataProvider invalidInputProvider
     */
    public function anExceptionIsThrownIfInputIsStructuredIncorrectly(string $input): void
    {
        $this->expectException(InvalidJsonWebTokenException::class);

        $validator = new JsonWebTokenValidator(
            new JsonWebKeyDtoCollection(),
            $this->getMockBuilder(DecoderInterface::class)->getMock(),
            $this->getMockBuilder(DecoderInterface::class)->getMock()
        );

        $validator->validate($input);
    }

    /**
     * @test
     */
    public function anExceptionIsThrownIfHeaderIsMissingTheKeyId(): void
    {
        $this->expectException(InvalidJsonWebTokenException::class);

        $validator = new JsonWebTokenValidator(
            new JsonWebKeyDtoCollection(),
            $this->getMockBuilder(DecoderInterface::class)->getMock(),
            $this->getMockBuilder(DecoderInterface::class)->getMock()
        );

        $validator->validate('eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.payload.signature');
    }

    private function getJsonWebKeyDtoCollectionWithMultipleKeys(): JsonWebKeyDtoCollection
    {
        $collection = new JsonWebKeyDtoCollection();

        $rsa = $this->getMockBuilder(RSA::class)
            ->getMock();

        $rsa->expects($this->once())
            ->method('verify')
            ->with('eyJhbGciOiJSUzI1NiIsImtpZCI6IjNjNTE0MzYyN2Y3NDFiOGVmM2ZhOWFmNWExODdhZGM3OGQwMDQ1MzgiLCJ0eXAiOiJKV1QifQ.pay_load', 'signature')
            ->willReturn(true);

        $dto = new JsonWebKeyDto('3c5143627f741b8ef3fa9af5a187adc78d004538', 'RS256', $rsa);

        $collection->add($dto);

        $rsa = $this->getMockBuilder(RSA::class)
            ->getMock();

        $dto = new JsonWebKeyDto('94d6ee7a5afaf393956a1631f3b610d1fb64dc9f', 'RS256', $rsa);

        $collection->add($dto);

        return $collection;
    }

    private function getJsonWebKeyDtoCollectionWithDtoThatReturnsFalseUponVerification(): JsonWebKeyDtoCollection
    {
        $collection = new JsonWebKeyDtoCollection();

        $rsa = $this->getMockBuilder(RSA::class)
            ->getMock();

        $rsa->expects($this->once())
            ->method('verify')
            ->with('eyJhbGciOiJSUzI1NiIsImtpZCI6IjNjNTE0MzYyN2Y3NDFiOGVmM2ZhOWFmNWExODdhZGM3OGQwMDQ1MzgiLCJ0eXAiOiJKV1QifQ.payload', 'signature')
            ->willReturn(false);

        $dto = new JsonWebKeyDto('3c5143627f741b8ef3fa9af5a187adc78d004538', 'RS256', $rsa);

        $collection->add($dto);

        return $collection;
    }

    private function getJsonWebKeyDtoCollectionWithDtoThatThrowsAnExceptionBecauseTheSignatureIsInvalid(): JsonWebKeyDtoCollection
    {
        $collection = new JsonWebKeyDtoCollection();

        $rsa = $this->getMockBuilder(RSA::class)
            ->getMock();

        $rsa->expects($this->once())
            ->method('verify')
            ->with('eyJhbGciOiJSUzI1NiIsImtpZCI6IjNjNTE0MzYyN2Y3NDFiOGVmM2ZhOWFmNWExODdhZGM3OGQwMDQ1MzgiLCJ0eXAiOiJKV1QifQ.payload', 'invalid_signature')
            ->willThrowException(new Exception('Invalid signature'));

        $dto = new JsonWebKeyDto('3c5143627f741b8ef3fa9af5a187adc78d004538', 'RS256', $rsa);

        $collection->add($dto);

        return $collection;
    }

    private function getBase64Decoder(): DecoderInterface
    {
        $decoder = $this->getMockBuilder(DecoderInterface::class)->getMock();

        $header = <<<HEADER
{
  "alg": "RS256",
  "kid": "3c5143627f741b8ef3fa9af5a187adc78d004538",
  "typ": "JWT"
}
HEADER;

        $decoder->expects($this->once())->method('decode')->willReturn($header);

        return $decoder;
    }

    private function getSanitizedBase64Decoder(): DecoderInterface
    {
        $decoder = $this->getMockBuilder(DecoderInterface::class)->getMock();

        $decoder->expects($this->once())->method('decode')->willReturn('signature');

        return $decoder;
    }

    private function getSanitizedBase64DecoderThatReturnsAnInvalidSignature(): DecoderInterface
    {
        $decoder = $this->getMockBuilder(DecoderInterface::class)->getMock();

        $decoder->expects($this->once())->method('decode')->willReturn('invalid_signature');

        return $decoder;
    }

    public function invalidInputProvider(): array
    {
        return array(
            array(
                'your'
            ),
            array(
                'json.web'
            ),
            array(
                't.o.k.e.n'
            ),
            array(
                'i.s..'
            ),
            array(
                'invalid '
            )
        );
    }
}