<?php

namespace Tests\BNNVARA\AkamaiClient\Unit\Application\Factory;

use BNNVARA\AkamaiClient\Application\Decoder\DecoderInterface;
use BNNVARA\AkamaiClient\Application\Decoder\SanitizedBase64Decoder;
use BNNVARA\AkamaiClient\Application\Factory\JsonWebKeyDtoCollectionFactory;
use BNNVARA\AkamaiClient\Domain\JsonWebKeyDto;
use BNNVARA\AkamaiClient\Domain\JsonWebKeyDtoCollection;
use phpseclib\Crypt\RSA;
use phpseclib\Math\BigInteger;
use PHPUnit\Framework\TestCase;

class JsonWebKeyDtoCollectionFactoryTest extends TestCase
{
    /** @test */
    public function itBuildsAJsonWebKeyDtoCollectionFromJsonWebKeys(): void
    {
        $factory = new JsonWebKeyDtoCollectionFactory(new SanitizedBase64Decoder);

        $json = <<<JSON
{
	"keys": [{
		"use": "sig",
		"kty": "RSA",
		"kid": "3c5143627f741b8ef3fa9af5a187adc78d004538",
		"alg": "RS256",
		"n": "Pz8_Pz8_Pz8_Pz8",
		"e": "YWJjZGVmZw"
	}]
}
JSON;

        $collection = $factory->build($json);

        $this->assertInstanceOf(JsonWebKeyDtoCollection::class, $collection);
        $this->assertCount(1, $collection);

        $this->assertInstanceOf(JsonWebKeyDto::class, $collection[0]);
        $this->assertEquals('3c5143627f741b8ef3fa9af5a187adc78d004538', $collection[0]->getId());

        $actualRSA = $collection[0]->getRSA();

        $this->assertInstanceOf(RSA::class, $actualRSA);
        $this->assertEquals($this->getExpectedRSA(), $actualRSA);

        $this->assertEquals('3c5143627f741b8ef3fa9af5a187adc78d004538', $collection[0]->getId());
        $this->assertEquals('RS256', $collection[0]->getAlgorithm());
    }

    private function getExpectedRSA(): RSA
    {
        $rsa = new RSA();

        $rsa->loadKey([
            'n' => new BigInteger('???????????', 256),
            'e' => new BigInteger('abcdefg', 256)
        ]);
        $rsa->setHash('sha256');
        $rsa->setSignatureMode(RSA::SIGNATURE_PKCS1);

        return $rsa;
    }
}
