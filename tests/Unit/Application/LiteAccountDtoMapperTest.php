<?php

namespace Tests\BNNVARA\AkamaiClient\Unit\Application;

use BNNVARA\AkamaiClient\Application\LiteAccountDtoMapper;
use BNNVARA\AkamaiClient\Domain\LiteAccountDto;
use DateTime;
use PHPUnit\Framework\TestCase;

class LiteAccountDtoMapperTest extends TestCase
{
    /** @test */
    public function aSingleAccountApiResultIsMappedToALiteAccountDto(): void
    {
        $singleAccountJson = file_get_contents(__DIR__ . '/../../Fixtures/singleAccountApiResult_LiteAccount.json');
        $dtoMapper = new LiteAccountDtoMapper('userLite');

        $accountDto = $dtoMapper->mapSingleAccountJsonToSingleAccountDto($singleAccountJson);
        $this->verifyLiteAccount($accountDto);
    }

    /** @test */
    public function aMultipleAccountApiResultIsMappedToALiteAccountDto(): void
    {
        $multipleAccountJson = file_get_contents(__DIR__ . '/../../Fixtures/multipleAccountSingleApiResult_LiteAccount.json');
        $dtoMapper = new LiteAccountDtoMapper('userLite');

        $accountDto = $dtoMapper->mapMultipleAccountJsonToSingleAccountDto($multipleAccountJson);
        $this->verifyLiteAccount($accountDto);
    }

    /** @test */
    public function aMinimalApiResultIsMappedToALiteAccountDto(): void
    {
        $multipleAccountJson = file_get_contents(__DIR__ . '/../../Fixtures/minimalAccountJson_LiteAccount.json');
        $dtoMapper = new LiteAccountDtoMapper('userLite');

        $accountDto = $dtoMapper->mapMultipleAccountJsonToSingleAccountDto($multipleAccountJson);
        $this->verifyMinimalLiteAccount($accountDto);
    }

    /**
     *  @test
     *  @dataProvider getAccountDtos
     */
    public function aLiteAccountDtoIsMappedToAValidQueryString(LiteAccountDto $accountDto, string $expectedString): void
    {
        $dtoMapper = new LiteAccountDtoMapper('userLite');
        $urlParameters = $dtoMapper->mapAccountDtoToSingleAccountString($accountDto, true);

        $this->assertEquals($expectedString, $urlParameters);
    }

    /**
     *  @test
     *  @dataProvider getAccountDtosWithoutUuids
     */
    public function aLiteAccountDtoIsMappedToAValidQueryStringWithoutUuid(LiteAccountDto $accountDto, string $expectedString): void
    {
        $dtoMapper = new LiteAccountDtoMapper('userLite');
        $urlParameters = $dtoMapper->mapAccountDtoToSingleAccountString($accountDto, false);

        $this->assertEquals($expectedString, $urlParameters);
    }

    public function getAccountDtos(): array
    {
        return [
            [
                $this->getLiteAccountDtoWithoutPropertyIds(),
                'uuid=195edeae-529f-47af-a23d-f910e173391a&type_name=userLite&include_record=true&attributes=%7B%22email%22%3A%22myemail%40example.com%22%2C%22subscriptions%22%3A%7B%22newsletters%22%3A%5B%7B%22doubleOptInStatus%22%3A%22Confirmed%22%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%2C%22name%22%3A%22DWDD%22%7D%5D%7D%7D'
            ],
            [
                $this->getLiteAccountDtoWithPropertyIds(),
                'uuid=195edeae-529f-47af-a23d-f910e173391a&type_name=userLite&include_record=true&attributes=%7B%22email%22%3A%22myemail%40example.com%22%2C%22subscriptions%22%3A%7B%22newsletters%22%3A%5B%7B%22id%22%3A%222563%22%2C%22doubleOptInStatus%22%3A%22Confirmed%22%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%2C%22name%22%3A%22DWDD%22%7D%5D%7D%7D'
            ]
        ];
    }

    public function getAccountDtosWithoutUuids(): array
    {
        return [
            [
                $this->getLiteAccountDtoWithoutPropertyIds(),
                'type_name=userLite&include_record=true&attributes=%7B%22email%22%3A%22myemail%40example.com%22%2C%22subscriptions%22%3A%7B%22newsletters%22%3A%5B%7B%22doubleOptInStatus%22%3A%22Confirmed%22%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%2C%22name%22%3A%22DWDD%22%7D%5D%7D%7D'
            ],
            [
                $this->getLiteAccountDtoWithPropertyIds(),
                'type_name=userLite&include_record=true&attributes=%7B%22email%22%3A%22myemail%40example.com%22%2C%22subscriptions%22%3A%7B%22newsletters%22%3A%5B%7B%22id%22%3A%222563%22%2C%22doubleOptInStatus%22%3A%22Confirmed%22%2C%22updated%22%3A%222010-12-12T12%3A12%3A12%2B00%3A00%22%2C%22name%22%3A%22DWDD%22%7D%5D%7D%7D'
            ]
        ];
    }

    private function verifyLiteAccount(LiteAccountDto $accountDto): void
    {
        $this->assertInstanceOf(LiteAccountDto::class, $accountDto);
        $this->assertEquals('Danny.eerens@bnnvara.nl', $accountDto->getEmail());
        $this->assertEquals('2020-05-18 08:52:43.736996 +0000', $accountDto->getLastUpdated());
        $this->assertEquals('bda59dc4-7412-4722-9fc5-94a450dcc924', $accountDto->getAccountId());
        $this->assertEquals('2020-05-14 12:35:57.733912 +0000', $accountDto->getCreatedDateTime());
        $this->assertEquals('2020-05-14 12:35:57.733912 +0000', $accountDto->getCreatedDateTime());

        $this->assertCount(1, $accountDto->getSubscriptions()->newsletters);
        $this->assertEquals('Confirmed', $accountDto->getSubscriptions()->newsletters[0]->doubleOptInStatus);
        $this->assertEquals('DWDD', $accountDto->getSubscriptions()->newsletters[0]->name);
        $this->assertEquals('4738', $accountDto->getSubscriptions()->newsletters[0]->id);
    }

    private function verifyMinimalLiteAccount(LiteAccountDto $accountDto): void
    {
        $this->assertInstanceOf(LiteAccountDto::class, $accountDto);
        $this->assertEquals('Danny.eerens@bnnvara.nl', $accountDto->getEmail());
        $this->assertEquals('2020-05-18 08:52:43.736996 +0000', $accountDto->getLastUpdated());
        $this->assertEquals('bda59dc4-7412-4722-9fc5-94a450dcc924', $accountDto->getAccountId());
        $this->assertEquals('2020-05-14 12:35:57.733912 +0000', $accountDto->getCreatedDateTime());
        $this->assertEquals('2020-05-14 12:35:57.733912 +0000', $accountDto->getCreatedDateTime());
        $this->assertCount(0, $accountDto->getSubscriptions()->newsletters);
    }

    private function getLiteAccountDtoWithoutPropertyIds(): LiteAccountDto
    {
        $datetime = new DateTime('2010-12-12 12:12:12');
        $formattedDateTime = $datetime->format('c');

        $subscription1 = new \stdClass();
        $subscription1->doubleOptInStatus = "Confirmed";
        $subscription1->updated = $datetime->format('c');
        $subscription1->name = "DWDD";

        $subscriptions = new \stdClass();
        $subscriptions->newsletters = [$subscription1];

        $primaryAddressStreetName = '';
        $primaryAddressHouseNumber = '';
        $primaryAddressHouseNumberAddition = '';
        $primaryAddressCountry = '';
        $primaryAddressCity = '';
        $primaryAddressZip = '';
        $relationNumber = 0;

        return new LiteAccountDto(
            '195edeae-529f-47af-a23d-f910e173391a',
            $formattedDateTime,
            'myemail@example.com',
            $formattedDateTime,
            $primaryAddressStreetName,
            $primaryAddressHouseNumber,
            $primaryAddressHouseNumberAddition,
            $primaryAddressCountry,
            $primaryAddressCity,
            $primaryAddressZip,
            $relationNumber,
            $subscriptions
        );
    }

    private function getLiteAccountDtoWithPropertyIds(): LiteAccountDto
    {
        $datetime = new DateTime('2010-12-12 12:12:12');
        $formattedDateTime = $datetime->format('c');

        $subscription1 = new \stdClass();
        $subscription1->id = '2563';
        $subscription1->doubleOptInStatus = 'Confirmed';
        $subscription1->updated = $datetime->format('c');
        $subscription1->name = 'DWDD';

        $subscriptions = new \stdClass();
        $subscriptions->newsletters = [$subscription1];

        $primaryAddressStreetName = '';
        $primaryAddressHouseNumber = '';
        $primaryAddressHouseNumberAddition = '';
        $primaryAddressCountry = '';
        $primaryAddressCity = '';
        $primaryAddressZip = '';
        $relationNumber = 0;

        return new LiteAccountDto(
            '195edeae-529f-47af-a23d-f910e173391a',
            $formattedDateTime,
            'myemail@example.com',
            $formattedDateTime,
            $primaryAddressStreetName,
            $primaryAddressHouseNumber,
            $primaryAddressHouseNumberAddition,
            $primaryAddressCountry,
            $primaryAddressCity,
            $primaryAddressZip,
            $relationNumber,
            $subscriptions
        );
    }
}
