<?php

namespace Tests\BNNVARA\AkamaiClient\Unit\Application\Decoder;

use BNNVARA\AkamaiClient\Application\Decoder\DecoderInterface;
use BNNVARA\AkamaiClient\Application\Decoder\Exception\ExpiredJsonWebTokenException;
use BNNVARA\AkamaiClient\Application\Decoder\JsonWebTokenDecoder;
use BNNVARA\AkamaiClient\Domain\JsonWebKeyDto;
use BNNVARA\AkamaiClient\Domain\JsonWebKeyDtoCollection;
use DateTime;
use Firebase\JWT\JWT;
use phpseclib\Crypt\RSA;
use PHPUnit\Framework\TestCase;

class JsonWebTokenDecoderTest extends TestCase
{
    /** @test */
    public function itDecodesAJsonWebToken()
    {
        $decoder = new JsonWebTokenDecoder(
            $this->getJsonWebKeyDtoCollectionWithMultipleKeys(),
            $this->getMockedBase64Decoder()
        );

        $authTime = new DateTime('1 second ago');
        $issuedAt = clone $authTime;
        $issuedAt->modify('+1 second');
        $expires = clone $issuedAt;
        $expires->modify('+1 hour');

        $token = $this->generateMockedJsonWebToken($authTime, $issuedAt, $expires);
        $expectedDecode = $this->decodeMockedJsonWebToken($token);

        $actualDecode = $decoder->decode($token);

        $this->assertEquals(
            $expectedDecode,
            $actualDecode
        );
    }

    /** @test */
    public function itThrowsAnExceptionUponEncounteringAnExpiredJsonWebToken()
    {
        $this->expectException(ExpiredJsonWebTokenException::class);

        $decoder = new JsonWebTokenDecoder(
            $this->getJsonWebKeyDtoCollectionWithMultipleKeys(),
            $this->getMockedBase64Decoder()
        );

        $authTime = new DateTime('2020-06-22 14:00');
        $issuedAt = new DateTime('2020-06-22 15:01');
        $expires = new DateTime('2020-06-22 15:00');

        $token = $this->generateMockedJsonWebToken($authTime, $issuedAt, $expires);

        $decoder->decode($token);
    }

    private function generateMockedJsonWebToken(
        DateTime $authTime,
        DateTime $issuedAt,
        DateTime $expires
    ) {
        $privateKeyForTesting = <<<PRIVATE_KEY
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAnzyis1ZjfNB0bBgKFMSvvkTtwlvBsaJq7S5wA+kzeVOVpVWw
kWdVha4s38XM/pa/yr47av7+z3VTmvDRyAHcaT92whREFpLv9cj5lTeJSibyr/Mr
m/YtjCZVWgaOYIhwrXwKLqPr/11inWsAkfIytvHWTxZYEcXLgAXFuUuaS3uF9gEi
NQwzGTU1v0FqkqTBr4B8nW3HCN47XUu0t8Y0e+lf4s4OxQawWD79J9/5d3Ry0vbV
3Am1FtGJiJvOwRsIfVChDpYStTcHTCMqtvWbV6L11BWkpzGXSW4Hv43qa+GSYOD2
QU68Mb59oSk2OB+BtOLpJofmbGEGgvmwyCI9MwIDAQABAoIBACiARq2wkltjtcjs
kFvZ7w1JAORHbEufEO1Eu27zOIlqbgyAcAl7q+/1bip4Z/x1IVES84/yTaM8p0go
amMhvgry/mS8vNi1BN2SAZEnb/7xSxbflb70bX9RHLJqKnp5GZe2jexw+wyXlwaM
+bclUCrh9e1ltH7IvUrRrQnFJfh+is1fRon9Co9Li0GwoN0x0byrrngU8Ak3Y6D9
D8GjQA4Elm94ST3izJv8iCOLSDBmzsPsXfcCUZfmTfZ5DbUDMbMxRnSo3nQeoKGC
0Lj9FkWcfmLcpGlSXTO+Ww1L7EGq+PT3NtRae1FZPwjddQ1/4V905kyQFLamAA5Y
lSpE2wkCgYEAy1OPLQcZt4NQnQzPz2SBJqQN2P5u3vXl+zNVKP8w4eBv0vWuJJF+
hkGNnSxXQrTkvDOIUddSKOzHHgSg4nY6K02ecyT0PPm/UZvtRpWrnBjcEVtHEJNp
bU9pLD5iZ0J9sbzPU/LxPmuAP2Bs8JmTn6aFRspFrP7W0s1Nmk2jsm0CgYEAyH0X
+jpoqxj4efZfkUrg5GbSEhf+dZglf0tTOA5bVg8IYwtmNk/pniLG/zI7c+GlTc9B
BwfMr59EzBq/eFMI7+LgXaVUsM/sS4Ry+yeK6SJx/otIMWtDfqxsLD8CPMCRvecC
2Pip4uSgrl0MOebl9XKp57GoaUWRWRHqwV4Y6h8CgYAZhI4mh4qZtnhKjY4TKDjx
QYufXSdLAi9v3FxmvchDwOgn4L+PRVdMwDNms2bsL0m5uPn104EzM6w1vzz1zwKz
5pTpPI0OjgWN13Tq8+PKvm/4Ga2MjgOgPWQkslulO/oMcXbPwWC3hcRdr9tcQtn9
Imf9n2spL/6EDFId+Hp/7QKBgAqlWdiXsWckdE1Fn91/NGHsc8syKvjjk1onDcw0
NvVi5vcba9oGdElJX3e9mxqUKMrw7msJJv1MX8LWyMQC5L6YNYHDfbPF1q5L4i8j
8mRex97UVokJQRRA452V2vCO6S5ETgpnad36de3MUxHgCOX3qL382Qx9/THVmbma
3YfRAoGAUxL/Eu5yvMK8SAt/dJK6FedngcM3JEFNplmtLYVLWhkIlNRGDwkg3I5K
y18Ae9n7dHVueyslrb6weq7dTkYDi3iOYRW8HRkIQh06wEdbxt0shTzAJvvCQfrB
jg/3747WSsf/zBTcHihTRBdAv6OmdhV4/dD5YBfLAkLrd+mX7iE=
-----END RSA PRIVATE KEY-----
PRIVATE_KEY;

        $header = <<<HEADER
{
  "alg": "RS256",
  "kid": "3c5143627f741b8ef3fa9af5a187adc78d004538",
  "typ": "JWT"
}
HEADER;

        $decodedHeader = json_decode($header);

        $payload = <<<PAYLOAD
{
  "at_hash": "1qO4LfshX8cD9r--cSBJ_A",
  "aud": [
    "9d2b490a-a3aa-47a5-b29d-d4a4256b2640"
  ],
  "auth_time": {$authTime->getTimestamp()},
  "exp": {$expires->getTimestamp()},
  "global_sub": "capture-v1://eu-dev-app.janraincapture.com/xxxxxxxxxxxxxxxxxxxxxxxxxx/userEntityName/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
  "iat": {$issuedAt->getTimeStamp()},
  "iss": "https://v1.api.eu.janrain.com/9129387e-c8c0-4c0c-82b9-906837e8a1ac/login",
  "jti": "xxxxxxxxxxxxxxxxxxxxxxxx",
  "sub": "0a25ffb6-da73-4aba-8d1f-52f78e07c3dd"
}
PAYLOAD;

        return JWT::encode(json_decode($payload, true), $privateKeyForTesting, 'RS256', $decodedHeader->kid, json_decode($header, true));
    }

    private function decodeMockedJsonWebToken(string $token): string
    {
        $publicKeyForTesting = $this->getPublicKeyForTesting();

        $decodedJWT = JWT::decode(
            $token,
            $publicKeyForTesting,
            array('RS256')
        );

        return json_encode($decodedJWT);
    }

    private function getJsonWebKeyDtoCollectionWithMultipleKeys(): JsonWebKeyDtoCollection
    {
        $collection = new JsonWebKeyDtoCollection();

        $rsa = $this->getMockBuilder(RSA::class)
            ->getMock();

        $rsa->expects($this->once())->method('__toString')->willReturn($this->getPublicKeyForTesting());

        $dto = new JsonWebKeyDto('3c5143627f741b8ef3fa9af5a187adc78d004538', 'RS256', $rsa);

        $collection->add($dto);

        $rsa = $this->getMockBuilder(RSA::class)
            ->getMock();

        $dto = new JsonWebKeyDto('94d6ee7a5afaf393956a1631f3b610d1fb64dc9f', 'RS256', $rsa);

        $collection->add($dto);

        return $collection;
    }

    private function getMockedBase64Decoder(): DecoderInterface
    {
        $decoder = $this->getMockBuilder(DecoderInterface::class)->getMock();

        $decodedHeader = <<<HEADER
{
  "alg": "RS256",
  "kid": "3c5143627f741b8ef3fa9af5a187adc78d004538",
  "typ": "JWT"
}
HEADER;

        $decoder->expects($this->once())->method('decode')->willReturn($decodedHeader);

        return $decoder;
    }

    private function getPublicKeyForTesting(): string
    {
        $publicKeyForTesting = <<<PUBLIC_KEY
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnzyis1ZjfNB0bBgKFMSv
vkTtwlvBsaJq7S5wA+kzeVOVpVWwkWdVha4s38XM/pa/yr47av7+z3VTmvDRyAHc
aT92whREFpLv9cj5lTeJSibyr/Mrm/YtjCZVWgaOYIhwrXwKLqPr/11inWsAkfIy
tvHWTxZYEcXLgAXFuUuaS3uF9gEiNQwzGTU1v0FqkqTBr4B8nW3HCN47XUu0t8Y0
e+lf4s4OxQawWD79J9/5d3Ry0vbV3Am1FtGJiJvOwRsIfVChDpYStTcHTCMqtvWb
V6L11BWkpzGXSW4Hv43qa+GSYOD2QU68Mb59oSk2OB+BtOLpJofmbGEGgvmwyCI9
MwIDAQAB
-----END PUBLIC KEY-----
PUBLIC_KEY;

        return $publicKeyForTesting;
    }
}