<?php

namespace Tests\BNNVARA\AkamaiClient\Unit\Application\Decoder;

use BNNVARA\AkamaiClient\Application\Decoder\SanitizedBase64Decoder;
use PHPUnit\Framework\TestCase;

class SanitizedBase64DecoderTest extends TestCase
{
    /** @test */
    public function itSanitizesAndBase64DecodesAString(): void
    {
        $sig = 'Pz8_Pz8_Pz8_Pz8';

        $decoder = new SanitizedBase64Decoder();

        $result = $decoder->decode($sig);

        $this->assertEquals('???????????', $result);
    }
}