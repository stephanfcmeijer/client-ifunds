<?php

namespace Tests\BNNVARA\AkamaiClient\Unit\Application\Decoder;

use BNNVARA\AkamaiClient\Application\Decoder\Base64Decoder;
use PHPUnit\Framework\TestCase;

class Base64DecoderTest extends TestCase
{
    /** @test */
    public function itBase64DecodesAString(): void
    {
        $string = <<<STRING
eyJhbGciOiJSUzI1NiIsImtpZCI6IjNjNTE0MzYyN2Y3NDFiOGVmM2ZhOWFmNWExODdhZGM3OGQwMDQ1MzgiLCJ0eXAiOiJKV1QifQ
STRING;

        $decoder = new Base64Decoder();

        $result = $decoder->decode($string);

        $decodedString = <<<DECODED_STRING
{"alg":"RS256","kid":"3c5143627f741b8ef3fa9af5a187adc78d004538","typ":"JWT"}
DECODED_STRING;

        $this->assertEquals($decodedString, $result);
    }
}