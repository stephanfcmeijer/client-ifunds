<?php

namespace Tests\BNNVARA\AkamaiClient\Unit\Infrastructure;

use BNNVARA\AkamaiClient\Application\AccountDtoMapper;
use BNNVARA\AkamaiClient\Application\Decoder\DecoderInterface;
use BNNVARA\AkamaiClient\Application\Encoder\EncoderInterface;
use BNNVARA\AkamaiClient\Application\Validator\Exception\InvalidJsonWebTokenException;
use BNNVARA\AkamaiClient\Application\Validator\ValidatorInterface;
use BNNVARA\AkamaiClient\Infrastructure\AkamaiOidcClient;
use PHPUnit\Framework\TestCase;

class AkamaiOidcClientTest extends TestCase
{
    /** @test */
    public function aJsonWebTokenIsValidated(): void
    {
        $mockedValidator = $this->getMockedValidatorThatConsidersAJsonWebTokenToBeValid();
        $jsonWebTokenDecoder = $this->getMockedDecoderThatIsNotCalled();

        $client = new AkamaiOidcClient(
            $mockedValidator,
            $jsonWebTokenDecoder
        );

        $this->assertTrue($client->validateJsonWebToken('json.web.token'));
    }

    /** @test */
    public function anExceptionIsThrownIfJsonWebTokenIsInvalid(): void
    {
        $mockedValidator = $this->getMockedValidatorThatConsidersAJsonWebTokenToBeInvalid();
        $jsonWebTokenDecoder = $this->getMockedDecoderThatIsNotCalled();

        $client = new AkamaiOidcClient(
            $mockedValidator,
            $jsonWebTokenDecoder
        );

        $this->expectException(InvalidJsonWebTokenException::class);

        $client->validateJsonWebToken('json.web.token');
    }

    /** @test */
    public function aJsonWebTokenIsExchangedForAnAccountId()
    {
        $mockedValidator = $this->getMockedValidatorThatConsidersAJsonWebTokenToBeValid();
        $mockedJsonWebTokenDecoder = $this->getMockedJsonWebTokenDecoderThatReturnsADecodedJsonWebToken();

        $client = new AkamaiOidcClient(
            $mockedValidator,
            $mockedJsonWebTokenDecoder
        );

        $accountId = $client->getAccountIdByJsonWebToken('json.web.token');

        $this->assertEquals('0a25ffb6-da73-4aba-8d1f-52f78e07c3dd', $accountId);
    }

    /** @test */
    public function anExceptionIsThrownIfTheJsonWebTokenUsedForRetrievalOfAnAccountIsInvalid()
    {
        $mockedValidator = $this->getMockedValidatorThatConsidersAJsonWebTokenToBeInvalid();
        $mockedJsonWebTokenDecoder = $this->getMockedDecoderThatIsNotCalled();

        $client = new AkamaiOidcClient(
            $mockedValidator,
            $mockedJsonWebTokenDecoder
        );

        $this->expectException(InvalidJsonWebTokenException::class);

        $client->getAccountIdByJsonWebToken('json.web.token');
    }

    private function getMockedValidatorThatConsidersAJsonWebTokenToBeValid(): ValidatorInterface
    {
        $validator = $this->getMockBuilder(ValidatorInterface::class)->getMock();

        $validator->expects($this->once())
            ->method('validate')
            ->with('json.web.token')
            ->willReturn(true);

        return $validator;
    }

    private function getMockedValidatorThatConsidersAJsonWebTokenToBeInvalid(): ValidatorInterface
    {
        $validator = $this->getMockBuilder(ValidatorInterface::class)->getMock();

        $validator->expects($this->once())
            ->method('validate')
            ->with('json.web.token')
            ->willReturn(false);

        return $validator;
    }

    private function getMockedJsonWebTokenDecoderThatReturnsADecodedJsonWebToken(): DecoderInterface
    {
        $decoder = $this->getMockBuilder(DecoderInterface::class)->getMock();

        $returnValue = <<<JSON
{
	"sub": "0a25ffb6-da73-4aba-8d1f-52f78e07c3dd"
}
JSON;
        $decoder->expects($this->once())
            ->method('decode')
            ->with('json.web.token')
            ->willReturn($returnValue);

        return $decoder;
    }

    private function getMockedDecoderThatIsNotCalled(): DecoderInterface
    {
        return $this->getMockBuilder(DecoderInterface::class)->getMock();
    }
}
