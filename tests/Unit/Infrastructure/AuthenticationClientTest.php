<?php

declare(strict_types=1);

namespace Tests\BNNVARA\AkamaiClient\Unit\Infrastructure;

use BNNVARA\AkamaiClient\Domain\AuthenticationClientInterface;
use BNNVARA\AkamaiClient\Domain\Exception\InvalidResponseException;
use BNNVARA\AkamaiClient\Domain\Exception\UnableToRetrieveAccountException;
use BNNVARA\AkamaiClient\Infrastructure\AuthenticationClient;
use Nyholm\Psr7\Request;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class AuthenticationClientTest extends TestCase
{
    /** @test */
    public function anAuthenticationClientCanBeCreated(): void
    {
        $client = new AuthenticationClient(
            $this->getNeverCalledClient(),
            'xxxxxxmockedclientidxxxxxx',
            'standardHL',
            '20200908135632026534',
            'signInForm',
            'en-US',
            'https://www.bnnvara.nl'
        );

        $this->assertInstanceOf(AuthenticationClientInterface::class, $client);
    }

    /** @test */
    public function validCredentialsReturnAnAuthenticatedUserDto(): void
    {
        $client = new AuthenticationClient(
            $this->createClientThatReturnsAResponse(
                $this->createDummyRequest(),
                $this->createResponseWithBody(
                    $this->createStreamWithContent(
                        file_get_contents(__DIR__ . '/Fixtures/AuthenticationResponse_success.json')
                    )
                )
            ),
            'xxxxxxmockedclientidxxxxxx',
            'standardHL',
            '20200908135632026534',
            'signInForm',
            'en-US',
            'https://www.bnnvara.nl'
        );

        $userAuthenticatedDto = $client->authenticateByEmailAddressAndPassword(
            'test@example.com',
            'testpassword'
        );

        $this->assertEquals('6c96d537-b556-4f2f-bb70-e89566044eaa', $userAuthenticatedDto->getAccountId());
    }

    /** @test */
    public function anExceptionIsThrownWhenInvalidCredentialsAreProvided(): void
    {
        $this->expectException(InvalidResponseException::class);

        $client = new AuthenticationClient(
            $this->createClientThatReturnsAResponse(
                $this->createDummyRequest(),
                $this->createResponseWithBody(
                    $this->createStreamWithContent(
                        file_get_contents(__DIR__ . '/Fixtures/AuthenticationResponse_failed.json')
                    )
                )
            ),
            'xxxxxxmockedclientidxxxxxx',
            'standardHL',
            '20200908135632026534',
            'signInForm',
            'en-US',
            'https://www.bnnvara.nl'
        );

        $client->authenticateByEmailAddressAndPassword(
            'test@example.com',
            'testpassword'
        );
    }

    /** @test */
    public function unableToRetrieveExceptionIsThrownIfClientFails(): void
    {
        $this->expectException(UnableToRetrieveAccountException::class);

        $client = new AuthenticationClient(
            $this->createExceptionThrowingClient(),
            'xxxxxxmockedclientidxxxxxx',
            'standardHL',
            '20200908135632026534',
            'signInForm',
            'en-US',
            'https://www.bnnvara.nl'
        );

        $client->authenticateByEmailAddressAndPassword(
            'test@example.com',
            'testpassword'
        );
    }

    private function getNeverCalledClient(): ClientInterface
    {
        $mock = $this->getMockBuilder(ClientInterface::class)->getMock();
        $mock->expects($this->never())->method('sendRequest');

        /** @var ClientInterface $mock */
        return $mock;
    }

    private function createClientThatReturnsAResponse(
        Request $request,
        ResponseInterface $response
    ): ClientInterface
    {
        /** @var MockObject $mock */
        $mock = $this->createClientInterface();
        $mock->expects($this->once())
            ->method('sendRequest')
            ->with($request)
            ->willReturn($response);

        /** @var ClientInterface $mock */
        return $mock;
    }

    private function createExceptionThrowingClient(): ClientInterface
    {
        $clientException = $this->getMockBuilder(ClientExceptionInterface::class)->getMock();

        /** @var MockObject $client */
        $client = $this->createClientInterface();
        $client->expects($this->once())
            ->method('sendRequest')
            ->willThrowException($clientException);

        /** @var ClientInterface $client */
        return $client;
    }

    private function createStreamInterface(): StreamInterface
    {
        $stream = $this->getMockBuilder(StreamInterface::class)->getMock();

        /** @var StreamInterface $stream */
        return $stream;
    }

    private function createResponseInterface(): ResponseInterface
    {
        $response = $this->getMockBuilder(ResponseInterface::class)->getMock();

        /** @var ResponseInterface $response */
        return $response;
    }

    private function createClientInterface(): ClientInterface
    {
        $client = $this->getMockBuilder(ClientInterface::class)->getMock();
        return $client;
    }

    private function createStreamWithContent(string $content): StreamInterface
    {
        /** @var MockObject $stream */
        $stream = $this->createStreamInterface();
        $stream->expects($this->any())
            ->method('getContents')
            ->willReturn($content);

        /** @var StreamInterface $stream */
        return $stream;
    }

    private function createResponseWithBody(StreamInterface $stream): ResponseInterface
    {
        /** @var MockObject $response */
        $response = $this->createResponseInterface();
        $response->expects($this->any())->method('getBody')->willReturn($stream);

        /** @var ResponseInterface $response */
        return $response;
    }

    private function createDummyRequest(): Request
    {
        return new Request(
            'POST',
            '/oauth/auth_native_traditional',
            [
                'form_params' => [
                    'client_id' => 'xxxxxxmockedclientidxxxxxx',
                    'flow' => 'standardHL',
                    'flow_version' => '20200908135632026534',
                    'form' => 'signInForm',
                    'signInEmailAddress' => 'test@example.com',
                    'currentPassword' => 'testpassword',
                    'locale' => 'en-US',
                    'redirect_uri' => 'https://www.bnnvara.nl',
                    'response_type' => 'token'
                ]
            ]
        );
    }
}
