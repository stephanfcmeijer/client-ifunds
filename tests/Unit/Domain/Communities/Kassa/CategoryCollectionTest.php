<?php

namespace Tests\BNNVARA\AkamaiClient\Unit\Domain\Communities\Kassa;

use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\CarsTransport;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\CategoryCollection;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\ElectronicsTelevision;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\FoodHealth;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\HolidayTravel;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\HousekeepingEnergy;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\LeisureGarden;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\MoneyLaw;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\ShopsEcommerce;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\WarrantyInsurance;
use PHPUnit\Framework\TestCase;

class CategoryCollectionTest extends TestCase
{
    /** @test */
    public function categoriesCanBeAddedToTheCollection()
    {
        $collection = new CategoryCollection();

        $collection->add(new HousekeepingEnergy(true));
        $collection->add(new ElectronicsTelevision(true));
        $collection->add(new LeisureGarden(true));
        $collection->add(new WarrantyInsurance(true));
        $collection->add(new FoodHealth(true));
        $collection->add(new HolidayTravel(true));
        $collection->add(new CarsTransport(true));
        $collection->add(new MoneyLaw(true));
        $collection->add(new ShopsEcommerce(true));

        $this->assertCount(9, $collection);
    }
}