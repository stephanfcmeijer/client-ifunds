<?php

namespace Tests\BNNVARA\AkamaiClient\Unit\Domain;

use BNNVARA\AkamaiClient\Domain\LiteAccountDto;
use PHPUnit\Framework\TestCase;

class LiteAccountDtoTest extends TestCase
{
    /** @test */
    public function aLiteAccountDtoCanBeCreated(): void
    {
        $subscription1 = new \stdClass();
        $subscription1->id = '2563';
        $subscription1->doubleOptInStatus = 'Confirmed';
        $subscription1->updated = '2020-05-11 10:00:00 +0100';
        $subscription1->name = 'DWDD';

        $subscriptions = new \stdClass();
        $subscriptions->newsletters = [$subscription1];

        $primaryAddressStreetName = 'teststraat';
        $primaryAddressHouseNumber = '1';
        $primaryAddressHouseNumberAddition = '';
        $primaryAddressCountry = 'nederland';
        $primaryAddressCity = 'teststad';
        $primaryAddressZip = '1234AB';
        $relationNumber = 12345678;

        $account = new LiteAccountDto(
            '12345678-1234-1234-1234-123456789012',
            '2020-05-11 10:00:00 +0100',
            'test@example.com',
            '2020-05-12 10:00:00 +0100',
            $primaryAddressStreetName,
            $primaryAddressHouseNumber,
            $primaryAddressHouseNumberAddition,
            $primaryAddressCountry,
            $primaryAddressCity,
            $primaryAddressZip,
            $relationNumber,
            $subscriptions
        );

        $this->assertInstanceOf(LiteAccountDto::class, $account);
        $this->assertEquals('12345678-1234-1234-1234-123456789012', $account->getAccountId());
        $this->assertEquals('2020-05-11 10:00:00 +0100', $account->getCreatedDateTime());
        $this->assertEquals('test@example.com', $account->getEmail());
        $this->assertEquals('2020-05-12 10:00:00 +0100', $account->getLastUpdated());

        $this->assertEquals($primaryAddressStreetName, $account->getPrimaryAddressStreetName());
        $this->assertEquals($primaryAddressHouseNumber, $account->getPrimaryAddressHouseNumber());
        $this->assertEquals($primaryAddressHouseNumberAddition, $account->getPrimaryAddressHouseNumberAddition());
        $this->assertEquals($primaryAddressCountry, $account->getPrimaryAddressCountry());
        $this->assertEquals($primaryAddressCity, $account->getPrimaryAddressCity());
        $this->assertEquals($primaryAddressZip, $account->getPrimaryAddressZip());
        $this->assertEquals($relationNumber, $account->getRelationNumber());

        $this->assertCount(1, $account->getSubscriptions()->newsletters);
        $this->assertEquals('Confirmed', $account->getSubscriptions()->newsletters[0]->doubleOptInStatus);
        $this->assertNotNull($account->getSubscriptions()->newsletters[0]->updated);
        $this->assertEquals('DWDD', $account->getSubscriptions()->newsletters[0]->name);
        $this->assertEquals('2563', $account->getSubscriptions()->newsletters[0]->id);
    }

    /** @test */
    public function aLiteAccountDtoWithNoAccountIdCanBeCreated(): void
    {
        $subscription1 = new \stdClass();
        $subscription1->id = '2563';
        $subscription1->doubleOptInStatus = 'Confirmed';
        $subscription1->updated = '2020-05-11 10:00:00 +0100';
        $subscription1->name = 'DWDD';

        $subscriptions = new \stdClass();
        $subscriptions->newsletters = [$subscription1];

        $primaryAddressStreetName = 'teststraat';
        $primaryAddressHouseNumber = '1';
        $primaryAddressHouseNumberAddition = '';
        $primaryAddressCountry = 'nederland';
        $primaryAddressCity = 'teststad';
        $primaryAddressZip = '1234AB';
        $relationNumber = 12345678;

        $account = new LiteAccountDto(
            null,
            '2020-05-11 10:00:00 +0100',
            'test@example.com',
            '2020-05-12 10:00:00 +0100',
            $primaryAddressStreetName,
            $primaryAddressHouseNumber,
            $primaryAddressHouseNumberAddition,
            $primaryAddressCountry,
            $primaryAddressCity,
            $primaryAddressZip,
            $relationNumber,
            $subscriptions
        );

        $this->assertInstanceOf(LiteAccountDto::class, $account);
        $this->assertNull($account->getAccountId());
        $this->assertEquals('2020-05-11 10:00:00 +0100', $account->getCreatedDateTime());
        $this->assertEquals('test@example.com', $account->getEmail());
        $this->assertEquals('2020-05-12 10:00:00 +0100', $account->getLastUpdated());

        $this->assertEquals($primaryAddressStreetName, $account->getPrimaryAddressStreetName());
        $this->assertEquals($primaryAddressHouseNumber, $account->getPrimaryAddressHouseNumber());
        $this->assertEquals($primaryAddressHouseNumberAddition, $account->getPrimaryAddressHouseNumberAddition());
        $this->assertEquals($primaryAddressCountry, $account->getPrimaryAddressCountry());
        $this->assertEquals($primaryAddressCity, $account->getPrimaryAddressCity());
        $this->assertEquals($primaryAddressZip, $account->getPrimaryAddressZip());
        $this->assertEquals($relationNumber, $account->getRelationNumber());

        $this->assertCount(1, $account->getSubscriptions()->newsletters);
        $this->assertEquals('Confirmed', $account->getSubscriptions()->newsletters[0]->doubleOptInStatus);
        $this->assertNotNull($account->getSubscriptions()->newsletters[0]->updated);
        $this->assertEquals('DWDD', $account->getSubscriptions()->newsletters[0]->name);
        $this->assertEquals('2563', $account->getSubscriptions()->newsletters[0]->id);
    }
}
