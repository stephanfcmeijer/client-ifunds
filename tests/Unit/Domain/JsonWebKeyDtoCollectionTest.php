<?php

namespace Tests\BNNVARA\AkamaiClient\Unit\Domain;

use BNNVARA\AkamaiClient\Domain\JsonWebKeyDto;
use BNNVARA\AkamaiClient\Domain\JsonWebKeyDtoCollection;
use phpseclib\Crypt\RSA;
use PHPUnit\Framework\TestCase;

class JsonWebKeyDtoCollectionTest extends TestCase
{
    /** @test */
    public function aJsonWebKeyDtoCanBeAddedToACollection(): void
    {
        $dtoCollection = new JsonWebKeyDtoCollection();

        $dtoCollection->add(
            new JsonWebKeyDto(
                '3c5143627f741b8ef3fa9af5a187adc78d004538',
                'RS256',
                new RSA()
            )
        );

        $this->assertCount( 1, $dtoCollection);
    }

    /** @test */
    public function itReturnsAJsonWebKeyDtoById(): void
    {
        $dtoCollection = new JsonWebKeyDtoCollection();

        $dtoCollection->add(
            new JsonWebKeyDto(
                '3c5143627f741b8ef3fa9af5a187adc78d004538',
                'RS256',
                new RSA()
            )
        );

        $dtoCollection->add(
            new JsonWebKeyDto(
                '94d6ee7a5afaf393956a1631f3b610d1fb64dc9f',
                'RS256',
                new RSA()
            )
        );

        $dtoCollection->add(
            new JsonWebKeyDto(
                '711a13a0be81b2dd623b142a17bd9231011a5989',
                'RS256',
                new RSA()
            )
        );

        $webKeyDto = $dtoCollection->getById('711a13a0be81b2dd623b142a17bd9231011a5989');

        $this->assertInstanceOf(JsonWebKeyDto::class, $webKeyDto);
        $this->assertEquals('711a13a0be81b2dd623b142a17bd9231011a5989', $webKeyDto->getId());
    }
}