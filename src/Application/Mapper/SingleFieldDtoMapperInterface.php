<?php

namespace BNNVARA\AkamaiClient\Application\Mapper;

use BNNVARA\AkamaiClient\Domain\SingleFieldDtoInterface;

interface SingleFieldDtoMapperInterface
{
    public function mapSingleFieldToSingleAccountString(SingleFieldDtoInterface $dto): string;

    public function mapSingleAccountJsonToSingleFieldDto(string $singleAccountJson): SingleFieldDtoInterface;
}