<?php

namespace BNNVARA\AkamaiClient\Application\Mapper;

use BNNVARA\AkamaiClient\Domain\Communities\CommunitiesDto;
use BNNVARA\AkamaiClient\Domain\Communities\CommunityCollectionInterface;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\CarsTransport;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\CategoryCollection;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\ComputersTelecom;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\ElectronicsTelevision;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\FoodHealth;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\HolidayTravel;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\HousekeepingEnergy;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\LeisureGarden;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\MoneyLaw;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\ShopsEcommerce;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\WarrantyInsurance;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\KassaDto;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications\NotificationCollection;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications\NotifyNewAnswers;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications\NotifyNewQuestions;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications\NotifyNewReactions;
use BNNVARA\AkamaiClient\Domain\Communities\Vroegevogels\VroegevogelsDto;
use BNNVARA\AkamaiClient\Domain\SingleFieldDtoInterface;

class CommunitiesDtoMapper implements SingleFieldDtoMapperInterface
{
    private string $akamaiUserEntityName;

    public function __construct(string $akamaiUserEntityName)
    {
        $this->akamaiUserEntityName = $akamaiUserEntityName;
    }

    public function mapSingleFieldToSingleAccountString(SingleFieldDtoInterface $dto): string
    {
        $kassa = $dto->getKassa();
        $vroegevogels = $dto->getVroegevogels();

        $categories = $this->collectionToArray($kassa->getCategories());

        $notifications = $this->collectionToArray($kassa->getNotifications());

        $communitiesJson = json_encode(
            array(
                'communities' => array(
                    'kassa' => array(
                        'showProfile' => $kassa->showProfile(),
                        'categories' => $categories,
                        'notifications' => $notifications
                    ),
                    'vroegevogels' => array(
                        'showProfile' => $vroegevogels->showProfile()
                    )
                )
            )
        );

        return http_build_query(
            array(
                'uuid' => $dto->getAccountId(),
                'type_name' => $this->akamaiUserEntityName,
                'include_record' => 'true',
                'attributes' => $communitiesJson
            )
        );
    }

    public function mapSingleAccountJsonToSingleFieldDto(string $singleAccountJson): SingleFieldDtoInterface
    {
        $apiAccountObject = json_decode($singleAccountJson);

        $communities = $apiAccountObject->result->communities;

        $categories = $this->getCategoryCollection();
        $notifications = $this->getNotificationCollection();
        $showProfileKassa = true;
        $showProfileVV = true;

        if (is_object($communities->kassa)) {
            $showProfileKassa = $communities->kassa->showProfile;
            foreach ($communities->kassa->categories as $categoryName => $value) {
                foreach ($categories as $categoryDto) {
                    if ($categoryName === $categoryDto::NAME) {
                        $categoryDto->setValue($value);
                    }
                }
            }

            foreach ($communities->kassa->notifications as $notificationName => $value) {
                foreach ($notifications as $notificationDto) {
                    if ($notificationName === $notificationDto::NAME) {
                        $notificationDto->setValue($value);
                    }
                }
            }
        }

        if (is_object($communities->vroegevogels)) {
            $showProfileVV = $communities->vroegevogels->showProfile;
        }

        $kassaDto = new KassaDto($categories, $notifications);
        $vvDto = new VroegevogelsDto();

        $kassaDto->setShowProfile($showProfileKassa);
        $vvDto->setShowProfile($showProfileVV);

        $communities = new CommunitiesDto(
            $apiAccountObject->result->uuid,
            $kassaDto,
            $vvDto
        );

        return $communities;
    }

    private function getCategoryCollection(): CategoryCollection
    {
        $categories = new CategoryCollection();

        $categories->add(new HousekeepingEnergy());
        $categories->add(new ElectronicsTelevision());
        $categories->add(new LeisureGarden());
        $categories->add(new WarrantyInsurance());
        $categories->add(new FoodHealth());
        $categories->add(new HolidayTravel());
        $categories->add(new CarsTransport());
        $categories->add(new MoneyLaw());
        $categories->add(new ShopsEcommerce());
        $categories->add(new ComputersTelecom());

        return $categories;
    }

    private function getNotificationCollection(): NotificationCollection
    {
        $notifications = new NotificationCollection();
        $notifications->add(new NotifyNewAnswers());
        $notifications->add(new NotifyNewQuestions());
        $notifications->add(new NotifyNewReactions());

        return $notifications;
    }

    private function collectionToArray(CommunityCollectionInterface $collection): array
    {
        $array = array();

        foreach ($collection as $element) {
            $array[$element::NAME] = $element->getValue();
        }

        return $array;
    }
}