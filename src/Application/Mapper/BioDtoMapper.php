<?php

namespace BNNVARA\AkamaiClient\Application\Mapper;

use BNNVARA\AkamaiClient\Domain\BioDto;
use BNNVARA\AkamaiClient\Domain\SingleFieldDtoInterface;

class BioDtoMapper implements SingleFieldDtoMapperInterface
{
    private string $akamaiUserEntityName;

    public function __construct(string $akamaiUserEntityName)
    {
        $this->akamaiUserEntityName = $akamaiUserEntityName;
    }

    public function mapSingleFieldToSingleAccountString(SingleFieldDtoInterface $dto): string
    {
        $bioJson = json_encode(
            array(
                'bio' => $dto->getValue()
            )
        );

        return http_build_query(
            array(
                'uuid' => $dto->getAccountId(),
                'type_name' => $this->akamaiUserEntityName,
                'include_record' => 'true',
                'attributes' => $bioJson
            )
        );
    }

    public function mapSingleAccountJsonToSingleFieldDto(string $singleAccountJson): SingleFieldDtoInterface
    {
        $apiAccountObject = json_decode($singleAccountJson);

        $bio = $apiAccountObject->result->bio;

        $dto = new BioDto(
            $apiAccountObject->result->uuid,
            $bio
        );

        return $dto;
    }
}