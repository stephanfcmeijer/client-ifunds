<?php

namespace BNNVARA\AkamaiClient\Application\Validator;

interface ValidatorInterface
{
    public function validate(string $string): bool;
}