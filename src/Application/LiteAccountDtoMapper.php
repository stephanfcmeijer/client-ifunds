<?php

namespace BNNVARA\AkamaiClient\Application;

use BNNVARA\AkamaiClient\Domain\DtoInterface;
use BNNVARA\AkamaiClient\Domain\Exception\InvalidJsonException;
use BNNVARA\AkamaiClient\Domain\Exception\InvalidNumberOfAccountsException;
use BNNVARA\AkamaiClient\Domain\LiteAccountDto;

class LiteAccountDtoMapper implements DtoMapperInterface
{
    private $userEntity;

    public function __construct(string $userEntity)
    {
        $this->userEntity = $userEntity;
    }

    public function mapSingleAccountJsonToSingleAccountDto(string $singleAccountJson): LiteAccountDto
    {
        $apiAccountObject = json_decode($singleAccountJson);

        if ($apiAccountObject === null) {
            throw new InvalidJsonException();
        }

        return $this->createLiteAccountDtoFromApiAccountObject($apiAccountObject->result);
    }

    public function mapMultipleAccountJsonToSingleAccountDto(string $multipleAccountJson): LiteAccountDto
    {
        $apiAccountObject = json_decode($multipleAccountJson);

        if ($apiAccountObject === null) {
            throw new InvalidJsonException();
        }

        if (count($apiAccountObject->results) > 1) {
            throw new InvalidNumberOfAccountsException('Expected 1, got' . count($apiAccountObject->results));
        }

        return $this->createLiteAccountDtoFromApiAccountObject($apiAccountObject->results[0]);
    }

    public function mapAccountDtoToSingleAccountString(DtoInterface $accountDto, bool $includeUuid): string
    {
        $subscriptions = $this->getSubscriptionsFromAccountDto($accountDto);

        $parameters = [
            'email' => $accountDto->getEmail(),
            'subscriptions' => $subscriptions
        ];

        /**
         *  @todo refactor this to a prettier solution, currently a string without uuid is used for
         *  the Create endpoint, while the string with uuid is used for the update and get endpoints.
         */
        if ($includeUuid) {
            return http_build_query(
                [
                    'uuid' => $accountDto->getAccountId(),
                    'type_name' => $this->userEntity,
                    'include_record' => 'true',
                    'attributes' => json_encode($parameters)
                ]
            );
        } else {
            return http_build_query(
                [
                    'type_name' => $this->userEntity,
                    'include_record' => 'true',
                    'attributes' => json_encode($parameters)
                ]
            );
        }
    }

    private function getSubscriptionsFromAccountDto(LiteAccountDto $accountDto): array
    {
        $subscriptions = [];
        if (property_exists($accountDto->getSubscriptions(), 'newsletters')) {
            $newsletters = [];
            foreach ($accountDto->getSubscriptions()->newsletters as $newsletterObject)
            {
                $newsletter = [];
                if (property_exists($newsletterObject, 'id')) {
                    $newsletter['id'] = $newsletterObject->id;
                }
                $newsletter['doubleOptInStatus'] = $newsletterObject->doubleOptInStatus;
                $newsletter['updated'] = $newsletterObject->updated;
                $newsletter['name'] = $newsletterObject->name;
                array_push($newsletters, $newsletter);
            }
            $subscriptions['newsletters'] = $newsletters;
        }

        return $subscriptions;
    }

    private function createLiteAccountDtoFromApiAccountObject(object $apiAccountObject): LiteAccountDto
    {
        return new LiteAccountDto(
            (string) $apiAccountObject->uuid,
            (string) $apiAccountObject->created,
            (string) $apiAccountObject->email,
            (string) $apiAccountObject->lastUpdated,
            (string) $apiAccountObject->primaryAddress->streetname,
            (string) $apiAccountObject->primaryAddress->housenumber,
            (string) $apiAccountObject->primaryAddress->housenumberAddition,
            (string) $apiAccountObject->primaryAddress->country,
            (string) $apiAccountObject->primaryAddress->city,
            (string) $apiAccountObject->primaryAddress->zip,
            (int) $apiAccountObject->relationNumber,
            $apiAccountObject->subscriptions
        );
    }
}
