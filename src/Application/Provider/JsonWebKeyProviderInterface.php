<?php

namespace BNNVARA\AkamaiClient\Application\Provider;

use BNNVARA\AkamaiClient\Domain\JsonWebKeyDtoCollection;

interface JsonWebKeyProviderInterface
{
    public function getJsonWebKeys(): JsonWebKeyDtoCollection;
}