<?php

namespace BNNVARA\AkamaiClient\Application;

use BNNVARA\AkamaiClient\Domain\DtoInterface;

interface DtoMapperInterface
{
    public function mapSingleAccountJsonToSingleAccountDto(string $singleAccountJson): DtoInterface;
    public function mapMultipleAccountJsonToSingleAccountDto(string $multipleAccountJson): DtoInterface;
    public function mapAccountDtoToSingleAccountString(DtoInterface $accountDto, bool $includeUuid): string;
}