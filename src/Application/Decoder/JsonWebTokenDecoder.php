<?php

namespace BNNVARA\AkamaiClient\Application\Decoder;

use BNNVARA\AkamaiClient\Application\Decoder\Exception\ExpiredJsonWebTokenException;
use BNNVARA\AkamaiClient\Domain\JsonWebKeyDtoCollection;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;

class JsonWebTokenDecoder implements DecoderInterface
{
    private JsonWebKeyDtoCollection $jsonWebKeys;
    private DecoderInterface $base64Decoder;

    const JWT_HEADER_INDEX = 0;

    public function __construct(
        JsonWebKeyDtoCollection $jsonWebKeys,
        DecoderInterface $base64Decoder
    ) {
        $this->jsonWebKeys = $jsonWebKeys;
        $this->base64Decoder = $base64Decoder;
    }

    /** @throws ExpiredJsonWebTokenException */
    public function decode(string $stringToDecode): string
    {
        $deconstructedToken = $this->deconstructToken($stringToDecode);

        $header = $deconstructedToken[self::JWT_HEADER_INDEX];

        $keyId = $this->getKeyIdFromHeader($header);

        $jsonWebKey = $this->jsonWebKeys->getById($keyId);

        try {
            $decodedJWT = JWT::decode(
                $stringToDecode,
                (string) $jsonWebKey->getRSA(),
                array($jsonWebKey->getAlgorithm())
            );
        } catch (ExpiredException $e) {
            throw new ExpiredJsonWebTokenException('Expired token');
        }

        return json_encode($decodedJWT);
    }

    private function deconstructToken(string $jsonWebToken): array
    {
        return explode('.', $jsonWebToken);
    }

    private function getKeyIdFromHeader(string $header): string
    {
        $decodedHeader = $this->base64Decoder->decode($header);
        $headerObject = json_decode($decodedHeader);

        return $headerObject->kid;
    }
}