<?php

namespace BNNVARA\AkamaiClient\Application\Decoder;

class SanitizedBase64Decoder implements DecoderInterface
{
    public function decode(string $stringToDecode): string
    {
        $stringToDecode = $this->sanitizeString($stringToDecode);
        return base64_decode($stringToDecode);
    }

    private function sanitizeString(string $string): string
    {
        return str_replace(['-','_'], ['+','/'], $string);
    }
}