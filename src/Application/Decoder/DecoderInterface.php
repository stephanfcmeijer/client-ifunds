<?php

namespace BNNVARA\AkamaiClient\Application\Decoder;

interface DecoderInterface
{
    public function decode(string $stringToDecode): string;
}