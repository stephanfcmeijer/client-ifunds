<?php

namespace BNNVARA\AkamaiClient\Application\Decoder;

class Base64Decoder implements DecoderInterface
{
    public function decode(string $stringToDecode): string
    {
        return base64_decode($stringToDecode);
    }
}