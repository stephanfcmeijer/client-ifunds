<?php

namespace BNNVARA\AkamaiClient\Application\Factory;

use BNNVARA\AkamaiClient\Domain\JsonWebKeyDtoCollection;

interface JsonWebKeyDtoCollectionFactoryInterface
{
    public function build(string $string): JsonWebKeyDtoCollection;
}