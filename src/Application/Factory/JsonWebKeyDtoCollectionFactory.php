<?php

namespace BNNVARA\AkamaiClient\Application\Factory;

use BNNVARA\AkamaiClient\Application\Decoder\DecoderInterface;
use BNNVARA\AkamaiClient\Domain\JsonWebKeyDto;
use BNNVARA\AkamaiClient\Domain\JsonWebKeyDtoCollection;
use phpseclib\Crypt\RSA;
use phpseclib\Math\BigInteger;
use stdClass;

class JsonWebKeyDtoCollectionFactory implements JsonWebKeyDtoCollectionFactoryInterface
{
    private DecoderInterface $sanitizedDecoder;

    const JWK_MODULUS_INDEX = 'n';
    const JWK_EXPONENT_INDEX = 'e';
    const JWK_BASE_NUMBER = 256;

    public function __construct(DecoderInterface $sanitizedDecoder)
    {
        $this->sanitizedDecoder = $sanitizedDecoder;
    }

    public function build(string $string): JsonWebKeyDtoCollection
    {
        $obj = json_decode($string);

        $collection = new JsonWebKeyDtoCollection();

        foreach ($obj->keys as $key) {
            $jsonWebKey = new JsonWebKeyDto(
                $key->kid,
                $key->alg,
                $this->buildRSAFromJsonWebKey($key)
            );
            $collection->append($jsonWebKey);
        }

        return $collection;
    }

    private function buildRSAFromJsonWebKey(stdClass $key): RSA
    {
        $rsa = new RSA();

        $jsonWebKeyModulusIndex = self::JWK_MODULUS_INDEX;
        $jsonWebKeyExponentIndex = self::JWK_EXPONENT_INDEX;

        $rsa->loadKey([
            self::JWK_MODULUS_INDEX => new BigInteger(
                $this->sanitizedDecoder->decode($key->$jsonWebKeyModulusIndex),
                self::JWK_BASE_NUMBER
            ),
            self::JWK_EXPONENT_INDEX => new BigInteger(
                $this->sanitizedDecoder->decode($key->$jsonWebKeyExponentIndex),
                self::JWK_BASE_NUMBER
            )
        ]);

        $rsa->setHash('sha256');
        $rsa->setSignatureMode(RSA::SIGNATURE_PKCS1);

        return $rsa;
    }
}