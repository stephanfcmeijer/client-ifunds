<?php

namespace BNNVARA\AkamaiClient\Application\Encoder;

class Base64Encoder implements EncoderInterface
{
    public function encode(string $stringToEncode): string
    {
        return base64_encode($stringToEncode);
    }
}