<?php

namespace BNNVARA\AkamaiClient\Application\Encoder;

interface EncoderInterface
{
    public function encode(string $stringToEncode): string;
}