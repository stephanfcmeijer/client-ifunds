<?php

namespace BNNVARA\AkamaiClient\Application;

use BNNVARA\AkamaiClient\Domain\DtoInterface;
use BNNVARA\AkamaiClient\Domain\Exception\InvalidJsonException;
use BNNVARA\AkamaiClient\Domain\Exception\InvalidNumberOfAccountsException;
use BNNVARA\AkamaiClient\Domain\SocialAccountDto;

class SocialAccountDtoMapper implements DtoMapperInterface
{
    private $akamaiEntityName;

    public function __construct(string $akamaiEntityName)
    {
        $this->akamaiEntityName = $akamaiEntityName;
    }

    /** @throws InvalidJsonException */
    public function mapSingleAccountJsonToSingleAccountDto(string $singleAccountJson): SocialAccountDto
    {
        $apiAccountObject = json_decode($singleAccountJson);

        if ($apiAccountObject === null) {
            throw new InvalidJsonException();
        }

        return $this->createAccountDtoFromApiAccountObject($apiAccountObject->result);
    }

    /** @throws InvalidNumberOfAccountsException
     *  @throws InvalidJsonException
     */
    public function mapMultipleAccountJsonToSingleAccountDto(string $multipleAccountJson): SocialAccountDto
    {
        $apiAccountObject = json_decode($multipleAccountJson);

        if ($apiAccountObject === null) {
            throw new InvalidJsonException();
        }

        if (count($apiAccountObject->results) > 1) {
            throw new InvalidNumberOfAccountsException('Expected 1, got ' . count($apiAccountObject->results));
        }

        return $this->createAccountDtoFromApiAccountObject($apiAccountObject->results[0]);
    }

    public function mapAccountDtoToSingleAccountString(DtoInterface $accountDto, bool $includeUuid): string
    {
        $consents = [
            'tracking' =>
                [
                    'granted' => $accountDto->getHasTrackingConsent(),
                    'updated' => $accountDto->getTrackingConsentUpdatedDateTime()
                ],
            'marketing' =>
                [
                    'granted' => $accountDto->getHasMarketingConsent(),
                    'updated' => $accountDto->getMarketingConsentUpdatedDateTime()
                ]
        ];

        $dateOfBirth = [
            'birthYear' => $accountDto->getBirthYear(),
            'birthMonth' => $accountDto->getBirthMonth(),
            'birthDay' => $accountDto->getBirthDay(),
        ];

        $primaryAddress = [
            'streetname' => $accountDto->getPrimaryAddressStreetName(),
            'housenumber' => $accountDto->getPrimaryAddressHouseNumber(),
            'housenumberAddition' => $accountDto->getPrimaryAddressHouseNumberAddition(),
            'city' => $accountDto->getPrimaryAddressCity(),
            'zip' => $accountDto->getPrimaryAddressZip(),
            'country' => $accountDto->getPrimaryAddressCountry()
        ];

        $parameters = [
            'givenName' => $accountDto->getGivenName(),
            'middleName' => $accountDto->getMiddleName(),
            'familyName' => $accountDto->getFamilyName(),
            'username' => $accountDto->getUsername(),
            'displayName' => $accountDto->getDisplayName(),
            'bio' => $accountDto->getBio(),
            'firstContact' => $accountDto->getFirstContact(),
            'emails' => $this->getEmails($accountDto->getEmails()),
            'email' => $accountDto->getEmail(),
            'emailVerified' => $accountDto->getEmailVerified(),
            'primaryAddress' => $primaryAddress,
            'registrationSource' => $accountDto->getRegistrationSource(),
            'dateOfBirth' => $dateOfBirth,
            'isActive' => $accountDto->isActive(),
            'isLite' => $accountDto->isLite(),
            'isRegistered' => $accountDto->isRegistered(),
            'isLockedOut' => $accountDto->isLockedOut(),
            'isVerified' => $accountDto->isVerified(),
            'isBanned' => $accountDto->isBanned(),
            'contactNumber' => $accountDto->getContactNumber(),
            'relationNumber' => $accountDto->getRelationNumber(),
            'externalId' => $accountDto->getExternalId(),
            'gender' => $accountDto->getGender(),
            'consents' => $consents,
            'photos' => $this->getPhotos($accountDto->getPhotos()),
            'phones' => $this->getPhones($accountDto->getPhones()),
            'legalAcceptances' => $this->getLegalAcceptances($accountDto->getLegalAcceptances()),
            'subscriptions' => $this->getSubscriptions($accountDto->getSubscriptions()),
            'communities' => $accountDto->getCommunities()
        ];

        if ($includeUuid) {
            return http_build_query(
                [
                    'uuid' => $accountDto->getAccountId(),
                    'type_name' => $this->akamaiEntityName,
                    'include_record' => 'true',
                    'attributes' => json_encode($parameters)
                ]
            );
        } else {
            return http_build_query(
                [
                    'type_name' => $this->akamaiEntityName,
                    'include_record' => 'true',
                    'attributes' => json_encode($parameters)
                ]
            );
        }
    }

    private function getPhones(array $phoneCollection): array
    {
        $phones = [];
        foreach ($phoneCollection as $phoneObject) {
            $phone = [];
            if (property_exists($phoneObject, 'id')) {
                $phone['id'] = $phoneObject->id;
            }
            $phone['number'] = $phoneObject->number;
            array_push($phones, $phone);
        }

        return $phones;
    }

    private function getEmails(array $emailCollection): array
    {
        $emails = [];
        foreach ($emailCollection as $emailObject) {
            $email = [];
            if (property_exists($emailObject, 'id')) {
                $email['id'] = $emailObject->id;
            }
            $email['address'] = $emailObject->address;
            $email['preferred'] = $emailObject->preferred;
            array_push($emails, $email);
        }

        return $emails;
    }

    private function getSubscriptions(object $subscriptionCollection): array
    {
        $subscriptions = [];
        if (property_exists($subscriptionCollection, 'newsletters')) {
            $newsletters = [];
            foreach ($subscriptionCollection->newsletters as $newsletterObject)
            {
                $newsletter = [];
                if (property_exists($newsletterObject, 'id')) {
                    $newsletter['id'] = $newsletterObject->id;
                }
                $newsletter['doubleOptInStatus'] = $newsletterObject->doubleOptInStatus;
                $newsletter['updated'] = $newsletterObject->updated;
                $newsletter['name'] = $newsletterObject->name;
                array_push($newsletters, $newsletter);
            }
            $subscriptions['newsletters'] = $newsletters;
        }

        return $subscriptions;
    }

    private function getLegalAcceptances(array $legalAcceptancesCollection): array
    {
        $legalAcceptances = [];
        foreach ($legalAcceptancesCollection as $legalAcceptanceObject) {
            $legalAcceptance = [];
            if (property_exists($legalAcceptanceObject, 'id')) {
                $legalAcceptance['id'] = $legalAcceptanceObject->id;
            }
            $legalAcceptance['dateAccepted'] = $legalAcceptanceObject->dateAccepted;
            $legalAcceptance['hasAcceptedTerms'] = $legalAcceptanceObject->hasAcceptedTerms;
            $legalAcceptance['legalAcceptanceId'] = $legalAcceptanceObject->legalAcceptanceId;
            array_push($legalAcceptances, $legalAcceptance);
        }

        return $legalAcceptances;
    }

    private function getPhotos(array $photoCollection): array
    {
        $photos = [];
        foreach ($photoCollection as $photoObject) {
            $photo = [];
            if (property_exists($photoObject, 'id')) {
                $photo['id'] = $photoObject->id;
            }
            $photo['type'] = $photoObject->type;
            $photo['value'] = $photoObject->value;
            array_push($photos, $photo);
        }

        return $photos;
    }

    private function createAccountDtoFromApiAccountObject(object $apiAccountObject): SocialAccountDto
    {
        return new SocialAccountDto(
            (string) $apiAccountObject->uuid,
            (string) $apiAccountObject->givenName,
            (string) $apiAccountObject->middleName,
            (string) $apiAccountObject->familyName,
            $apiAccountObject->username,
            (string) $apiAccountObject->displayName,
            (string) $apiAccountObject->bio,
            $apiAccountObject->isVerified ?? false,
            $apiAccountObject->isRegistered ?? false,
            $apiAccountObject->isLockedOut ?? false,
            $apiAccountObject->isBanned ?? false,
            $apiAccountObject->isActive ?? true,
            $apiAccountObject->isLite ?? true,
            $apiAccountObject->emails,
            (string) $apiAccountObject->email,
            $apiAccountObject->emailVerified,
            (string) $apiAccountObject->lastUpdated,
            (string) $apiAccountObject->firstContact,
            (string) $apiAccountObject->primaryAddress->streetname,
            (string) $apiAccountObject->primaryAddress->housenumber,
            (string) $apiAccountObject->primaryAddress->housenumberAddition,
            (string) $apiAccountObject->primaryAddress->country,
            (string) $apiAccountObject->primaryAddress->city,
            (string) $apiAccountObject->primaryAddress->zip,
            $apiAccountObject->consents->marketing->granted ?? false,
            $apiAccountObject->consents->marketing->updated,
            $apiAccountObject->consents->tracking->granted ?? false,
            $apiAccountObject->consents->tracking->updated,
            (string) $apiAccountObject->dateOfBirth->birthYear,
            (string) $apiAccountObject->dateOfBirth->birthMonth,
            (string) $apiAccountObject->dateOfBirth->birthDay,
            (string) $apiAccountObject->created,
            (string) $apiAccountObject->registrationSource,
            $apiAccountObject->password,
            $apiAccountObject->phones,
            $apiAccountObject->photos,
            (string) $apiAccountObject->externalId,
            (string) $apiAccountObject->contactNumber,
            (string) $apiAccountObject->relationNumber,
            $apiAccountObject->legalAcceptances,
            (string) $apiAccountObject->gender,
            $apiAccountObject->subscriptions,
            $apiAccountObject->communities
        );
    }
}