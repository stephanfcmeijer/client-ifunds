<?php
declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Infrastructure;

use BNNVARA\AkamaiClient\Domain\AuthenticationClientInterface;
use BNNVARA\AkamaiClient\Domain\Exception\InvalidResponseException;
use BNNVARA\AkamaiClient\Domain\AuthenticatedUserDto;
use BNNVARA\AkamaiClient\Domain\Exception\UnableToRetrieveAccountException;
use Nyholm\Psr7\Request;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;

class AuthenticationClient implements AuthenticationClientInterface
{
    private const STAT_OK_MESSAGE = 'ok';

    private ClientInterface $client;
    private string $clientId;
    private string $flow;
    private string $flowVersion;
    private string $formName;
    private string $locale;
    private string $redirectUrl;

    public function __construct(
        ClientInterface $client,
        string $clientId,
        string $flow,
        string $flowVersion,
        string $formName,
        string $locale,
        string $redirectUrl
    ) {
        $this->client = $client;
        $this->clientId = $clientId;
        $this->flow = $flow;
        $this->flowVersion = $flowVersion;
        $this->formName = $formName;
        $this->locale = $locale;
        $this->redirectUrl = $redirectUrl;
    }

    public function authenticateByEmailAddressAndPassword(string $emailAddress, string $password): AuthenticatedUserDto
    {
        $endpoint = '/oauth/auth_native_traditional';

        $options = [
            'form_params' => [
                'client_id' => $this->clientId,
                'flow' => $this->flow,
                'flow_version' => $this->flowVersion,
                'form' => $this->formName,
                'signInEmailAddress' => $emailAddress,
                'currentPassword' => $password,
                'locale' => $this->locale,
                'redirect_uri' => $this->redirectUrl,
                'response_type' => 'token'
            ]
        ];

        $accountData = $this->doCurlRequest($endpoint, $options);

        return $this->getDataFromAuthenticatedUser($accountData);
    }

    private function doCurlRequest(string $endpoint, array $options): string
    {
        try {
            $akamaiAccountJson = $this->client->sendRequest(new Request('POST', $endpoint, $options))->getBody()->getContents();
            $akamaiResponseObject = json_decode($akamaiAccountJson);

            if ($akamaiResponseObject->stat !== self::STAT_OK_MESSAGE) {
                throw new InvalidResponseException("Response is not ok: " . $akamaiAccountJson);
            }
        } catch(ClientExceptionInterface $e) {
            throw new UnableToRetrieveAccountException();
        }

        return $akamaiAccountJson;
    }

    private function getDataFromAuthenticatedUser(string $accountData): AuthenticatedUserDto
    {
        $parsedAccountData = json_decode($accountData);

        return new AuthenticatedUserDto(
            $parsedAccountData->capture_user->uuid
        );
    }
}
