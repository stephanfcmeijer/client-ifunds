<?php

namespace BNNVARA\AkamaiClient\Infrastructure;

use BNNVARA\AkamaiClient\Application\Decoder\DecoderInterface;
use BNNVARA\AkamaiClient\Application\Validator\Exception\InvalidJsonWebTokenException;
use BNNVARA\AkamaiClient\Application\Validator\ValidatorInterface;
use BNNVARA\AkamaiClient\Domain\AkamaiOidcClientInterface;

class AkamaiOidcClient implements AkamaiOidcClientInterface
{
    private ValidatorInterface $validator;
    private DecoderInterface $decoder;

    public function __construct(
        ValidatorInterface $validator,
        DecoderInterface $decoder
    ) {
        $this->validator = $validator;
        $this->decoder = $decoder;
    }

    public function validateJsonWebToken(string $jsonWebToken): bool
    {
        $isValid = $this->validator->validate($jsonWebToken);

        if ($isValid === false) {
            throw new InvalidJsonWebTokenException('Invalid token');
        }

        return $isValid;
    }

    public function getAccountIdByJsonWebToken(string $jsonWebToken): string
    {
        if ($this->validator->validate($jsonWebToken) === true) {

            $decodedToken = $this->decoder->decode($jsonWebToken);
            $tokenObject = json_decode($decodedToken);

            return $tokenObject->sub;
        } else {
            throw new InvalidJsonWebTokenException('Invalid token');
        }
    }
}