<?php

namespace BNNVARA\AkamaiClient\Domain;

use BNNVARA\AkamaiClient\Application\Validator\Exception\InvalidJsonWebTokenException;
use BNNVARA\AkamaiClient\Domain\Exception\UnableToRetrieveAccountException;

interface AkamaiOidcClientInterface
{
    /** @throws InvalidJsonWebTokenException */
    public function validateJsonWebToken(string $jsonWebToken): bool;

    /** @throws InvalidJsonWebTokenException */
    public function getAccountIdByJsonWebToken(string $jsonWebToken): string;
}