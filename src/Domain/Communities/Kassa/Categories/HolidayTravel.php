<?php

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories;

class HolidayTravel extends Category
{
    public const NAME = 'holiday_travel';
}