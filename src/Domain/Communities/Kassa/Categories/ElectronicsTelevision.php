<?php

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories;

class ElectronicsTelevision extends Category
{
    public const NAME = 'electronics_television';
}