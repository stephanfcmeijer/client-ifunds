<?php

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories;

class ShopsEcommerce extends Category
{
    public const NAME = 'shops_ecommerce';
}