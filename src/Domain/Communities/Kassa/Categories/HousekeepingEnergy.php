<?php

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories;

class HousekeepingEnergy extends Category
{
    public const NAME = 'housekeeping_energy';
}