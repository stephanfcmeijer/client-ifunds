<?php

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories;

class WarrantyInsurance extends Category
{
    public const NAME = 'warranty_insurance';
}