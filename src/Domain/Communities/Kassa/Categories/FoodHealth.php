<?php

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories;

class FoodHealth extends Category
{
    public const NAME = 'food_health';
}