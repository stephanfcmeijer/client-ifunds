<?php

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories;

class LeisureGarden extends Category
{
    public const NAME = 'leisure_garden';
}