<?php

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa;

use BNNVARA\AkamaiClient\Domain\Communities\CommunityCollectionInterface;
use BNNVARA\AkamaiClient\Domain\Communities\CommunityDto;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\CategoryCollection;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications\NotificationCollection;

class KassaDto extends CommunityDto
{
    private CategoryCollection $categories;
    private NotificationCollection $notifications;

    public function __construct(
        CommunityCollectionInterface $categories,
        CommunityCollectionInterface $notifications
    ) {
        $this->categories = $categories;
        $this->notifications = $notifications;
    }

    public function getCategories(): CommunityCollectionInterface
    {
        return $this->categories;
    }

    public function getNotifications(): CommunityCollectionInterface
    {
        return $this->notifications;
    }
}