<?php

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications;

use ArrayObject;
use BNNVARA\AkamaiClient\Domain\Communities\CommunityCollectionInterface;
use BNNVARA\AkamaiClient\Domain\Communities\CommunityElementInterface;

class NotificationCollection extends ArrayObject implements CommunityCollectionInterface
{
    public function add(CommunityElementInterface $element): void
    {
        parent::append($element);
    }
}