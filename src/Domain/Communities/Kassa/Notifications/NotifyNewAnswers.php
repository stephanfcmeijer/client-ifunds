<?php

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications;

class NotifyNewAnswers extends Notification
{
    public const NAME = 'notifyNewAnswers';
}