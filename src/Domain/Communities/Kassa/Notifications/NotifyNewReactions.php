<?php

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications;

class NotifyNewReactions extends Notification
{
    public const NAME = 'notifyNewReactions';
}