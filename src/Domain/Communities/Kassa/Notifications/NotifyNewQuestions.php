<?php

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications;

class NotifyNewQuestions extends Notification
{
    public const NAME = 'notifyNewQuestions';
}