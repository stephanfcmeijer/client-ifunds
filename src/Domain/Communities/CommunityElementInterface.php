<?php

namespace BNNVARA\AkamaiClient\Domain\Communities;

interface CommunityElementInterface
{
    public function getValue(): bool;
}