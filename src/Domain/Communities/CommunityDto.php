<?php

namespace BNNVARA\AkamaiClient\Domain\Communities;

abstract class CommunityDto
{
    protected bool $showProfile = true;

    public function showProfile(): bool
    {
        return $this->showProfile;
    }

    public function setShowProfile(bool $showProfile): void
    {
        $this->showProfile = $showProfile;
    }
}