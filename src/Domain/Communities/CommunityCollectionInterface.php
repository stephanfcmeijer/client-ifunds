<?php

namespace BNNVARA\AkamaiClient\Domain\Communities;

interface CommunityCollectionInterface
{
    public function add(CommunityElementInterface $element): void;
}