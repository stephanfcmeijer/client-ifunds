<?php

namespace BNNVARA\AkamaiClient\Domain;

use BNNVARA\AkamaiClient\Domain\Exception\InvalidResponseException;
use BNNVARA\AkamaiClient\Domain\Exception\UnableToRetrieveAccountException;

interface AkamaiClientInterface
{
    /** @throws UnableToRetrieveAccountException */
    public function getAccountByAccountId(string $accountId): DtoInterface;

    /** @throws UnableToRetrieveAccountException */
    public function getAccountByEmailAddress(string $emailAddress): DtoInterface;

    /** @throws UnableToRetrieveAccountException */
    public function getAccountByUsername(string $username): DtoInterface;

    /** @throws UnableToRetrieveAccountException */
    public function getAccountByRelationNumber(string $relationNumber): DtoInterface;

    /** @throws UnableToRetrieveAccountException */
    public function getAccountByExternalId(string $relationNumber): DtoInterface;

    /** @throws UnableToRetrieveAccountException */
    public function deleteByAccountId(string $accountId): void;

    /** @throws UnableToRetrieveAccountException */
    public function updateAccount(DtoInterface $accountDto): void;

    /** @throws InvalidResponseException if a bad status code is returned by Akamai */
    public function getAmount(): int;

    /** @throws InvalidResponseException if a bad status code is returned by Akamai */
    public function createLiteAccount(LiteAccountDto $liteAccountDto): void;

    public function updateSingleField(SingleFieldDtoInterface $singleFieldDto): void;

    public function getSingleFieldByAccountId(string $accountId): SingleFieldDtoInterface;
}