<?php

namespace BNNVARA\AkamaiClient\Domain;

class BioDto implements SingleFieldDtoInterface
{
    private string $accountId;
    private ?string $value;

    public function __construct(
        string $accountId,
        ?string $value
    ) {
        $this->accountId = $accountId;
        $this->value = $value;
    }

    public function getAccountId(): string
    {
        return $this->accountId;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): void
    {
        $this->value = $value;
    }
}