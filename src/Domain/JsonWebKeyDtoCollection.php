<?php

namespace BNNVARA\AkamaiClient\Domain;

use ArrayObject;

class JsonWebKeyDtoCollection extends ArrayObject
{
    public function add(JsonWebKeyDto $jsonWebKey): void
    {
        parent::append($jsonWebKey);
    }

    public function getById(string $id): JsonWebKeyDto
    {
        foreach ($this as $webKey) {
            if ($webKey->getId() === $id) {
                return $webKey;
            }
        }
    }
}