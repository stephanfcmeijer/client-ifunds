<?php

namespace BNNVARA\AkamaiClient\Domain;

class AccountDto implements DtoInterface
{
    private string $accountId;
    private string $givenName = '';
    private string $middleName = '';
    private string $familyName = '';
    private ?string $username = null;
    private string $displayName = '';
    private string $bio = '';
    private bool $isVerified;
    private bool $isRegistered;
    private bool $isLockedOut;
    private bool $isBanned;
    private bool $isActive;
    private bool $isLite;
    private array $emails = [];
    private string $email = '';
    private ?string $emailVerified = null;
    private string $lastUpdated = '';
    private string $firstContact = '';
    private string $primaryAddressStreetName = '';
    private string $primaryAddressHouseNumber = '';
    private string $primaryAddressHouseNumberAddition = '';
    private string $primaryAddressCountry = '';
    private string $primaryAddressCity = '';
    private string $primaryAddressZip = '';
    private bool $hasMarketingConsent;
    private ?string $marketingConsentUpdatedDateTime = null;
    private bool $hasTrackingConsent;
    private ?string $trackingConsentUpdatedDateTime = null;
    private string $birthYear = '';
    private string $birthMonth = '';
    private string $birthDay = '';
    private string $createdDateTime = '';
    private string $registrationSource = '';
    private ?object $password = null;
    private array $phones = [];
    private array $photos = [];
    private string $externalId  = '';
    private string $contactNumber  = '';
    private string $relationNumber  = '';
    private array $legalAcceptances = [];
    private string $gender  = '';
    private object $subscriptions;
    private object $communities;

    public function __construct(
        string $accountId,
        string $givenName,
        string $middleName,
        string $familyName,
        ?string $username,
        string $displayName,
        string $bio,
        bool $isVerified,
        bool $isRegistered,
        bool $isLockedOut,
        bool $isBanned,
        bool $isActive,
        bool $isLite,
        array $emails,
        string $email,
        ?string $emailVerified,
        string $lastUpdated,
        string $firstContact,
        string $primaryAddressStreetName,
        string $primaryAddressHouseNumber,
        string $primaryAddressHouseNumberAddition,
        string $primaryAddressCountry,
        string $primaryAddressCity,
        string $primaryAddressZip,
        bool $hasMarketingConsent,
        ?string $marketingConsentUpdatedDateTime,
        bool $hasTrackingConsent,
        ?string $trackingConsentUpdatedDateTime,
        string $birthYear,
        string $birthMonth,
        string $birthDay,
        string $createdDateTime,
        string $registrationSource,
        ?object $password,
        array $phones,
        array $photos,
        string $externalId,
        string $contactNumber,
        string $relationNumber,
        array $legalAcceptances,
        string $gender,
        object $subscriptions,
        object $communities
    ) {
        $this->accountId = $accountId;
        $this->givenName = $givenName;
        $this->middleName = $middleName;
        $this->familyName = $familyName;
        $this->username = $username;
        $this->displayName = $displayName;
        $this->bio = $bio;
        $this->isVerified = $isVerified;
        $this->isRegistered = $isRegistered;
        $this->isLockedOut = $isLockedOut;
        $this->isBanned = $isBanned;
        $this->isActive = $isActive;
        $this->isLite = $isLite;
        $this->emails = $emails;
        $this->email = $email;
        $this->emailVerified = $emailVerified;
        $this->lastUpdated = $lastUpdated;
        $this->firstContact = $firstContact;
        $this->primaryAddressStreetName = $primaryAddressStreetName;
        $this->primaryAddressHouseNumber = $primaryAddressHouseNumber;
        $this->primaryAddressHouseNumberAddition = $primaryAddressHouseNumberAddition;
        $this->primaryAddressCountry = $primaryAddressCountry;
        $this->primaryAddressCity = $primaryAddressCity;
        $this->primaryAddressZip = $primaryAddressZip;
        $this->hasMarketingConsent = $hasMarketingConsent;
        $this->marketingConsentUpdatedDateTime = $marketingConsentUpdatedDateTime;
        $this->hasTrackingConsent = $hasTrackingConsent;
        $this->trackingConsentUpdatedDateTime = $trackingConsentUpdatedDateTime;
        $this->birthYear = $birthYear;
        $this->birthMonth = $birthMonth;
        $this->birthDay = $birthDay;
        $this->createdDateTime = $createdDateTime;
        $this->registrationSource = $registrationSource;
        $this->password = $password;
        $this->phones = $phones;
        $this->photos = $photos;
        $this->externalId = $externalId;
        $this->contactNumber = $contactNumber;
        $this->relationNumber = $relationNumber;
        $this->legalAcceptances = $legalAcceptances;
        $this->gender = $gender;
        $this->subscriptions = $subscriptions;
        $this->communities = $communities;
    }

    public function getAccountId(): string
    {
        return $this->accountId;
    }

    public function getGivenName(): string
    {
        return $this->givenName;
    }

    public function getRegistrationSource(): string
    {
        return $this->registrationSource;
    }

    public function getBio(): string
    {
        return $this->bio;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function isRegistered(): bool
    {
        return $this->isRegistered;
    }

    public function isLockedOut(): bool
    {
        return $this->isLockedOut;
    }

    public function isBanned(): bool
    {
        return $this->isBanned;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function isLite(): bool
    {
        return $this->isLite;
    }

    public function getEmails(): array
    {
        return $this->emails;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getEmailVerified(): ?string
    {
        return $this->emailVerified;
    }

    public function getLastUpdated(): string
    {
        return $this->lastUpdated;
    }

    public function getMiddleName(): string
    {
        return $this->middleName;
    }

    public function getFirstContact(): string
    {
        return $this->firstContact;
    }

    public function getPrimaryAddressStreetName(): string
    {
        return $this->primaryAddressStreetName;
    }

    public function getPrimaryAddressHouseNumber(): string
    {
        return $this->primaryAddressHouseNumber;
    }

    public function getPrimaryAddressHouseNumberAddition(): string
    {
        return $this->primaryAddressHouseNumberAddition;
    }

    public function getPrimaryAddressCountry(): string
    {
        return $this->primaryAddressCountry;
    }

    public function getPrimaryAddressCity(): string
    {
        return $this->primaryAddressCity;
    }

    public function getPrimaryAddressZip(): string
    {
        return $this->primaryAddressZip;
    }

    public function getRelationNumber(): string
    {
        return $this->relationNumber;
    }

    public function getHasMarketingConsent(): bool
    {
        return $this->hasMarketingConsent;
    }

    public function getMarketingConsentUpdatedDateTime(): ?string
    {
        return $this->marketingConsentUpdatedDateTime;
    }

    public function getHasTrackingConsent(): bool
    {
        return $this->hasTrackingConsent;
    }

    public function getTrackingConsentUpdatedDateTime(): ?string
    {
        return $this->trackingConsentUpdatedDateTime;
    }

    public function getBirthYear(): string
    {
        return $this->birthYear;
    }

    public function getBirthMonth(): string
    {
        return $this->birthMonth;
    }

    public function getBirthDay(): string
    {
        return $this->birthDay;
    }

    public function getCreatedDateTime(): string
    {
        return $this->createdDateTime;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function getFamilyName(): string
    {
        return $this->familyName;
    }

    public function getPassword(): ?object
    {
        return $this->password;
    }

    public function getPhones(): array
    {
        return $this->phones;
    }

    public function getPhotos(): array
    {
        return $this->photos;
    }

    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    public function getExternalId(): string
    {
        return $this->externalId;
    }

    public function getContactNumber(): string
    {
        return $this->contactNumber;
    }

    public function getLegalAcceptances(): array
    {
        return $this->legalAcceptances;
    }

    public function getGender(): string
    {
        return $this->gender;
    }

    public function getSubscriptions(): object
    {
        return $this->subscriptions;
    }

    public function getCommunities(): object
    {
        return $this->communities;
    }
}