<?php
declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain;

interface AuthenticationClientInterface
{
    public function authenticateByEmailAddressAndPassword(string $emailAddress, string $password): AuthenticatedUserDto;
}