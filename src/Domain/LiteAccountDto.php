<?php

namespace BNNVARA\AkamaiClient\Domain;

class LiteAccountDto implements DtoInterface
{
    private ?string $accountId = null;
    private string $createdDateTime = '';
    private string $email = '';
    private string $lastUpdated = '';
    private string $primaryAddressStreetName = '';
    private string $primaryAddressHouseNumber = '';
    private string $primaryAddressHouseNumberAddition = '';
    private string $primaryAddressCountry = '';
    private string $primaryAddressCity = '';
    private string $primaryAddressZip = '';
    private int $relationNumber = 0;
    private object $subscriptions;

    public function __construct(
        ?string $accountId,
        string $createdDateTime,
        string $email,
        string $lastUpdated,
        string $primaryAddressStreetName,
        string $primaryAddressHouseNumber,
        string $primaryAddressHouseNumberAddition,
        string $primaryAddressCountry,
        string $primaryAddressCity,
        string $primaryAddressZip,
        int $relationNumber,
        object $subscriptions
    ) {
        $this->accountId = $accountId;
        $this->createdDateTime = $createdDateTime;
        $this->email = $email;
        $this->lastUpdated = $lastUpdated;
        $this->primaryAddressStreetName = $primaryAddressStreetName;
        $this->primaryAddressHouseNumber = $primaryAddressHouseNumber;
        $this->primaryAddressHouseNumberAddition = $primaryAddressHouseNumberAddition;
        $this->primaryAddressCountry = $primaryAddressCountry;
        $this->primaryAddressCity = $primaryAddressCity;
        $this->primaryAddressZip = $primaryAddressZip;
        $this->relationNumber = $relationNumber;
        $this->subscriptions = $subscriptions;
    }

    public function getRelationNumber(): int
    {
        return $this->relationNumber;
    }

    public function getAccountId(): ?string
    {
        return $this->accountId;
    }

    public function getCreatedDateTime(): string
    {
        return $this->createdDateTime;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getLastUpdated(): string
    {
        return $this->lastUpdated;
    }

    public function getSubscriptions(): object
    {
        return $this->subscriptions;
    }

    public function getPrimaryAddressStreetName(): string
    {
        return $this->primaryAddressStreetName;
    }

    public function getPrimaryAddressHouseNumber(): string
    {
        return $this->primaryAddressHouseNumber;
    }

    public function getPrimaryAddressHouseNumberAddition(): string
    {
        return $this->primaryAddressHouseNumberAddition;
    }

    public function getPrimaryAddressCountry(): string
    {
        return $this->primaryAddressCountry;
    }

    public function getPrimaryAddressCity(): string
    {
        return $this->primaryAddressCity;
    }

    public function getPrimaryAddressZip(): string
    {
        return $this->primaryAddressZip;
    }
}
