<?php

namespace BNNVARA\AkamaiClient\Domain;

interface SingleFieldDtoInterface
{
    public function getAccountId(): string;
}