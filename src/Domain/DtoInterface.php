<?php

namespace BNNVARA\AkamaiClient\Domain;

interface DtoInterface
{
    public function getEmail(): string;
}