<?php

namespace BNNVARA\AkamaiClient\Domain;

use phpseclib\Crypt\RSA;

class JsonWebKeyDto
{
    private string $id;
    private string $algorithm;
    private RSA $rsa;

    public function __construct(
        string $id,
        string $algorithm,
        RSA $rsa
    ) {
        $this->id = $id;
        $this->algorithm = $algorithm;
        $this->rsa = $rsa;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getAlgorithm(): string
    {
        return $this->algorithm;
    }

    public function getRSA(): RSA
    {
        return $this->rsa;
    }
}