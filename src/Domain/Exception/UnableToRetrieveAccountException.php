<?php

namespace BNNVARA\AkamaiClient\Domain\Exception;

use Exception;

class UnableToRetrieveAccountException extends Exception
{
}