<?php

namespace BNNVARA\AkamaiClient\Domain\Exception;

use Exception;

class InvalidJsonException extends Exception
{
}