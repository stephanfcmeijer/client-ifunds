<?php

namespace BNNVARA\AkamaiClient\Domain\Exception;

use Exception;

class InvalidNumberOfAccountsException extends Exception
{
}